//
//  RTKBBproPeripheral+Deprecated.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2021/9/18.
//

#import <RTKAudioConnectSDK/RTKBBproPeripheral.h>

NS_ASSUME_NONNULL_BEGIN

@interface RTKBBproPeripheral (Deprecated)


/**
 * Clear all cached state.
 *
 * @discussion This method will reset all properties for cache to nil.
 */
- (void)clearCachedState;


/**
 * Return the name of device.
 *
 * @discussion No by call CBPeripheral.name, this name is retrieved by message exchange through BBpro GATT Service, though they are often same.
 */
@property (nonatomic, readonly, nullable) NSString *LEName DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/// Return the name of this device used for BREDR controller.
@property (nonatomic, readonly, nullable) NSString *BREDRName DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the language this peripheral used currently for Voice prompt.
 */
@property (nonatomic, readonly, nullable) RTKBBproLanguageType currentLanguage DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of languages this peripheral support for used for Voice prompt.
 */
@property (nonatomic, readonly, nullable) NSSet <RTKBBproLanguageType> *supportedLanguages DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the battery level of the primary bud of a RWS pair or this device last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly, nullable) NSNumber *primaryBatteryLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the battery level of the secondary bud of a RWS pair or this device last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly, nullable) NSNumber *secondaryBatteryLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the battery level of the cradle last cached if the device contain cradle.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly, nullable) NSNumber *cradleBatteryLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the RWS on-off state of the this device last cached.
 *
 * @discussion Scalar value is of BOOL type.
 */
@property (nonatomic, nullable, readonly) NSNumber *RWSState DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the RWS channel state of the this device last cached.
 */
@property (nonatomic, readonly, nullable) NSNumber *RWSChannel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


/**
 * Get the RWS on-off state of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSState property when succeed.
 */
- (void)getRWSStateWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL state))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getRWSStateWithCompletionHandler: instead.");

/**
 * Get the RWS channel flow state of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSChannel property when succeed.
 */
- (void)getRWSChannelWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproChannelDirection channel))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getRWSChannelWithCompletionHandler: instead.");

/**
 * Switch the RWS channel state of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSChannel property when succeed.
 */
- (void)switchRWSChannelWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -switchRWSChannelWithCompletionHandler: instead.");


/**
 * Return the APT on-off state of the this device last cached.
 */
@property (nonatomic, nullable, readonly) NSNumber *APTState DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


/**
 * Return the DSP Information dictionary of the this device last cached.
 *
 * @discussion Deprecated by more recent device.
 */
@property (nonatomic, readonly, nullable) NSDictionary <NSString*,NSNumber*>* DSPInfo;

/**
 * The raw para data for DSP of this device. Not available.
 *
 * @discussion Deprecated by more recent device.
 */
@property (nonatomic, nullable) NSData *DSPEQData DEPRECATED_ATTRIBUTE;


@property (nonatomic, readonly, nullable) NSNumber *versionNumber;

- (void)getInfoWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, uint16_t ver))handler DEPRECATED_MSG_ATTRIBUTE("Use -getVersionInfoWithCompletionHandler: instead.");


- (void)getCapabilityWithCompletion:(nullable RTKLECompletionBlock)handler DEPRECATED_MSG_ATTRIBUTE("Use -getCapabilityWithCompletionHandler: instead.");


/**
 * Return the chip ID of the this device last cached.
 *
 * @discussion The value is of RTKBBproChip type.
 */
@property (nonatomic, readonly) NSNumber *chipID;

/**
 * Return the packageID of the this device last cached.
 */
@property (nonatomic, readonly) NSNumber *packageID;


#pragma mark - Device Name

/**
 * Get the LE name of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LEName property when succeed.
 */
- (void)getLENameWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, NSString *name))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getLENameWithCompletionHandler: instead.");

/**
 * Set a new LE name of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LEName property when succeed.
 */
- (void)setLEName:(NSString *)name withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -setLEName:withCompletionHandler: instead.");

/**
 * Get the BREDR name of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update BREDRName property when succeed.
 */
- (void)getBREDRNameWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, NSString *name))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getBREDRNameWithCompletionHandler: instead.");

/**
 * Set a new BREDR name of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update BREDRName property when succeed.
 */
- (void)setBREDRName:(NSString *)name withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -setBREDRName:withCompletionHandler: instead.");


#pragma mark - Language
/**
 * Get the current using language and supported languages of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update currentLanguage and supportedLanguages properties when succeed.
 */
- (void)getLanguageWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, NSSet <RTKBBproLanguageType>* supportedLangs, RTKBBproLanguageType currentLang))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getLanguageWithCompletionHandler: instead.");

/**
 * Set the current language of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update currentLanguage property when succeed.
 */
- (void)setCurrentLanguage:(RTKBBproLanguageType)lang withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -setCurrentLanguage:withCompletionHandler: instead.");


#pragma mark - Battery
/**
 * Get the current battery charge level of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully. @c RTKBBproBatteryLevelInvalid means this value is not valid.
 *
 * @discussion Will update primaryBatteryLevel and secondaryBatteryLevel properties when succeed.
 */
- (void)getBatteryLevelWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproBatteryLevel primary, RTKBBproBatteryLevel secondary))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getBatteryLevelWithCompletionHandler: instead.");



/**
 * Return the default role last cached of this device.
 *
 * @discussion Scalar value is of RTKBBproBudRole type.
 */
@property (nonatomic, readonly) NSNumber *defaultBudRole DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");



/**
 * Return the side last cached of this device.
 *
 * @discussion Scalar value is of RTKBBproBudSide type.
 */
@property (nonatomic, readonly) NSNumber *budSide DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");



@property (nonatomic, readonly) NSNumber *channelDirection DEPRECATED_ATTRIBUTE;
- (void)getRWSBudChannelDirectionWithCompletionHandler:(void(^)(BOOL success, NSError *_Nullable error, RTKBBproChannelDirection direction))handler DEPRECATED_ATTRIBUTE;



/**
 * Return the default channel last cached of this device.
 *
 * @discussion Scalar value is of RTKBBproChannelDirection type.
 */
@property (nonatomic, readonly) NSNumber *defaultChannelSide DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");



/**
 * Return the listening mode switch cycle last cached of this device.
 *
 * @discussion Scalar value is of RTKBBproListeningModeSwitchCycle type.
 */
@property (nonatomic, readonly) NSNumber *listeningSwitchCycle DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Get the Listening mode cycle of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update listeningSwitchCycle property when succeed.
 */
- (void)getListeningModeSwitchCycleStateWithCompletion:(nullable void (^)(BOOL success, NSError *_Nullable error, RTKBBproListeningModeSwitchCycle cycle))handler DEPRECATED_MSG_ATTRIBUTE("Use -getListeningModeSwitchCycleStateWithCompletionHandler: instead.");


#pragma mark - APT
/**
 * Get the APT on-off state of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTState property when succeed.
 */
- (void)getAPTStateWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL state))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getAPTStateWithCompletionHandler: instead.");

/**
 * Switch the APT on-off state of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTState property when succeed.
 */
- (void)switchAPTStateWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -switchAPTStateWithCompletionHandler: instead.");



#pragma mark - DSP
/**
 * Get DSP version information of BBpro Peripheral.
 * @param completionHandler Called upon completion. If success, info is a dictionary containning DSP Info element (a example next), else a error is provided. info Dictionary example:  @{@"Scenario": @(0x00), @"SF": @(0x03), @"ROM": @(0x01010101), @"RAM": @(0x01010101), @"Patch": @(0x01010101), @"SDK": @(0x01010101)}
 *
 * @discussion Deprecated by more recent device.
 */
- (void)getDSPInfoWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, NSDictionary <NSString*,NSNumber*>*_Nullable info))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getDSPInfoWithCompletionHandler: instead.");


/**
 * Set DSP EQ effect parameter.
 *
 * @param paramterData EQ raw data to send to DSP. The data should be return by call -[RTKBBproEQSetting parameterDataOfSampleRate:] method.
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 *
 * @discussion This method set equalizer of 44.1kHz sample rate audio. Deprecated by more recent device.
 */
- (void)setDSPEQ:(NSData *)paramterData withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -setDSPEQ:withCompletionHandler: instead.");

/**
 * Set DSP EQ effect parameter.
 *
 * @param paramterData EQ raw data to send to DSP. The data should be return by call -[RTKBBproEQSetting parameterDataOfSampleRate:] method.
 * @param sampleRate The sample rate to which this EQ affect.
 * @param handler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 *
 * @discussion Deprecated by more recent device.
 */
- (void)setDSPEQData:(NSData *)paramterData ofSampleRate:(RTKBBproSampleRate)sampleRate withCompletion:(nullable void(^)(BOOL, NSError*_Nullable))handler DEPRECATED_MSG_ATTRIBUTE("Use -setDSPEQData:ofSampleRate:withCompletionHandler: instead.");

/**
 * Clear DSP equalizer effect.
 *
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 *
 * @discussion Deprecated by more recent device.
 */
- (void)clearDSPEQWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -clearDSPEQWithCompletionHandler: instead.");

/**
 * Return the ANC on-off state last cached of this device.
 *
 * @discussion Scalar value is of BOOL type.
 */
@property (nonatomic, readonly) NSNumber *ANCEnabled DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the ANC scenario current used last cached of this device.
 *
 * @discussion Scalar value is of RTKBBproANCScenario type.
 */
@property (nonatomic, readonly) NSNumber *currentANCModeIndex DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of ANC modes last cached supported by this device.
 *
 * @discussion For each item, scalar value is of RTKBBproANCScenario type.
 */
@property (nonatomic, readonly) NSArray<NSNumber*> *ANCModes DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");



/**
 * Return the count of APT EQ entry last cached of this device.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly) NSNumber *ANCModeEQCount DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current used EQ index last cached of this device.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly) NSNumber *currentANCModeEQIndex DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings in remote device runing ANC mode.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (nonatomic, readonly) NSArray <RTKBBproEQSetting*> *ANCModeEQSettings DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

- (void)getANCStateWithCompletion:(nullable void (^)(BOOL success, NSError *_Nullable error, BOOL enabled, NSUInteger currentModeIndex, NSArray<NSNumber*> * modes))handler DEPRECATED_MSG_ATTRIBUTE("Use -getANCStateWithCompletionHandler: instead.");


@property (nonatomic, readonly) NSNumber *EQStatus;
- (void)getEQStatusWithCompletion:(void(^)(BOOL success, NSError*_Nullable error, BOOL isOn))completionHandler DEPRECATED_ATTRIBUTE;

- (void)setEQEnable:(BOOL)enable withCompletion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_ATTRIBUTE;


@property (nonatomic, readonly) NSNumber *EQCount DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
- (void)getEQEntryCountWithCompletion:(void(^)(BOOL success, NSError*_Nullable error, NSUInteger count))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getEQEntryCountOfMode:withCompletionHandler: instead.");

@property (nonatomic, readonly) NSNumber *EQIndex DEPRECATED_MSG_ATTRIBUTE("Use cache instead.");
- (void)getCurrentEQIndexWithCompletion:(void(^)(BOOL success, NSError*_Nullable error, NSUInteger EQIndex))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getCurrentEQIndexOfMode:withCompletionHandler: instead.");

- (void)setCurrentEQIndex:(NSUInteger)index completion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -setCurrentEQIndex:ofMode:withCompletionHandler: instead.");


/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings in remote device runing normal mode.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (nonatomic, readonly) NSArray <RTKBBproEQSetting*> *normalModeEQSettings;



- (void)getEQParameterOfIndex:(NSUInteger)index completion:(void(^)(BOOL success, NSError*_Nullable error, RTKBBproSampleRate sampleRate, NSData *_Nullable paraData))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -getEQParameterOfIndex:ofMode:withCompletionHandler: instead.");

- (void)setEQParameterOfIndex:(NSUInteger)index withData:(NSData *)data ofSampleRate:(RTKBBproSampleRate)sampleRate completion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler DEPRECATED_MSG_ATTRIBUTE("Use -setEQParameterOfIndex:ofMode:withData:ofSampleRate:completion: instead.");


- (void)getEQParameterAtIndex:(NSUInteger)idx toResolveEQSetting:(RTKBBproEQSetting *)setting completionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler DEPRECATED_MSG_ATTRIBUTE("Use -getEQParameterOfIndex:ofMode:toResolveEQSetting:withCompletionHandler: instead.");

- (void)setEQParameterAtIndex:(NSUInteger)idx ofSampleRate:(RTKBBproSampleRate)sampleRate withSetting:(RTKBBproEQSetting *)setting completionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler DEPRECATED_MSG_ATTRIBUTE("Use -setEQParameterAtIndex:ofMode:ofSampleRate:withSetting:completionHandler: instead.");

/**
 * Send the parameter data to a EQ setting entry specified by index of the specified mode.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)setEQParameterOfIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                     withData:(NSData *)data
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                   completion:(nullable RTKLECompletionBlock)handler DEPRECATED_MSG_ATTRIBUTE("Use -setEQParameterOfIndex:ofMode:withData:ofSampleRate:completionHandler: instead.");

/**
 * Return the global volume level of the this device last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *volumeLevel DEPRECATED_ATTRIBUTE;

/**
 * Return the global maximum volume level of the this device last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100. This value is often used to set upper limit on a UI control.
 */
@property (nonatomic, readonly) NSNumber *maxVolumeLevel  DEPRECATED_ATTRIBUTE;

/**
 * Get the global volume information of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update volumeLevel and maxVolumeLevel properties when succeed. Not availble anymore.
 */
- (void)getVolumeStatusWithCompletionHandler:(RTKLECompletionBlock)handler  DEPRECATED_ATTRIBUTE;

/**
 * Set the global volume level of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update volumeLevel property when succeed. Not availble anymore.
 */
- (void)setVolumeLevelTo:(uint8_t)level withCompletion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler  DEPRECATED_ATTRIBUTE;


/**
 * Return a boolean value which indicates if app could adjust the VP ringtone volume level of both bud simultaneously.
 *
 * @discussion This is useful to UI appearence.
 */
@property (nonatomic) BOOL VPRingtoneVolumeAdjustSimultaneously;

/**
 * Return the VP Ringtone volume level of the left side bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *LVPRingtoneVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the VP Ringtone volume level of the right side bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *RVPRingtoneVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the VP Ringtone minimum volume level of the left side bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *minLVPRingtoneVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the VP Ringtone maximum volume level of the left side bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *maxLVPRingtoneVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the VP Ringtone minimum volume level of the right side bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *minRVPRingtoneVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the VP Ringtone maximum volume level of the right side bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Range from 0 to 100.
 */
@property (nonatomic, readonly) NSNumber *maxRVPRingtoneVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");



/**
 * Return a list of MMI actions supported by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of RTKBBproPeripheralMMI type.
 */
@property (nonatomic, readonly, nullable) NSArray <NSNumber*> *supportedMMIs DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


/**
 * Return a list of MMI motions supported by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of RTKBBproPeripheralMMIClickType type.
 */
@property (nonatomic, readonly, nullable) NSArray <NSNumber*> *supportedClicks DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of call status for MMI supported by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of RTKBBproPeripheralMMIStatus type.
 */
@property (nonatomic, readonly, nullable) NSArray <NSNumber*> *supportedPhoneStatus DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of MMI mapping by this peripheral last cached.
 *
 * @discussion For each item, scalar value is of RTKBBproMMIMapping type.
 */
@property (nonatomic, readonly, nullable) NSArray <NSValue*> *MMIKeyMappings DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


/**
 * Return whether the button locked is on of this peripheral last cached.
 *
 * @discussion For each item, scalar value is of BOOL type.
 */
@property (nonatomic, readonly, nullable) NSNumber *buttonLocked DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");



/**
 * Return whether the gaming mode is on of this peripheral last cached.
 *
 * @discussion Scalar value is of BOOL type.
 */
@property (nonatomic, readonly) NSNumber *gamingModeEnabled DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the low lantency level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly) NSNumber *lowLatencyLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the maximum low lantency level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly) NSNumber *maxLowLatencyLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


/**
 * Return a boolean value which indicate if the specified mode is using by device.
 */
- (BOOL)isEQEnabledOfMode:(RTKBBproEQMode)mode  DEPRECATED_ATTRIBUTE;

@property (nonatomic, readonly) NSNumber *gamingModeEQCount;
- (void)getGamingModeEQEntryCountWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, NSUInteger count))handler DEPRECATED_MSG_ATTRIBUTE("Use -getEQEntryCountOfMode:withCompletionHandler: instead.");

@property (nonatomic, readonly) NSNumber *gamingModeEQIndex;
- (void)getCurrentGamingModeEQIndexWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, NSUInteger EQIndex))handler DEPRECATED_MSG_ATTRIBUTE("Use -getCurrentEQIndexOfMode:withCompletionHandler: instead.");

- (void)setCurrentGamingModeEQIndex:(NSUInteger)index completionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler DEPRECATED_MSG_ATTRIBUTE("Use -setCurrentEQIndex:ofMode:withCompletionHandler: instead.");

- (void)getGamingModeEQParameterOfIndex:(NSUInteger)index completionHandler:(void(^)(BOOL success, NSError*_Nullable error, RTKBBproSampleRate sampleRate, NSData *paraData))handler DEPRECATED_MSG_ATTRIBUTE("Use -getEQParameterOfIndex:ofMode:withCompletionHandler: instead.");

- (void)setGamingModeEQParameterOfIndex:(NSUInteger)index withData:(NSData *)data ofSampleRate:(RTKBBproSampleRate)sampleRate completionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler DEPRECATED_MSG_ATTRIBUTE("Use -setEQParameterOfIndex:ofMode:withData:ofSampleRate:completion: instead.");

- (void)getGamingModeEQParameterAtIndex:(NSUInteger)idx toResolveEQSetting:(RTKBBproEQSetting *)setting completionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler DEPRECATED_MSG_ATTRIBUTE("Use -getEQParameterOfIndex:ofMode:toResolveEQSetting:withCompletionHandler: instead.");

- (void)setGamingModeEQParameterAtIndex:(NSUInteger)idx ofSampleRate:(RTKBBproSampleRate)sampleRate withSetting:(RTKBBproEQSetting *)setting completionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler DEPRECATED_MSG_ATTRIBUTE("Use -setEQParameterAtIndex:ofMode:ofSampleRate:withSetting:completionHandler: instead.");

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings in remote device runing gaming mode.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (nonatomic, readonly) NSArray <RTKBBproEQSetting*> *gamingModeEQSettings DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


#pragma mark - LLAPT
/**
 * Return the LLAPT on-off state last cached of this device.
 *
 * @discussion Scalar value is of BOOL type.
 */
@property (nonatomic, readonly) NSNumber *LLAPTEnabled DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
/**
 * Return the LLAPT scenario current used last cached of this device.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, nullable) NSNumber *currentLLAPTScenarioIndex DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of LLAPT scenarios last cached supported by this device.
 *
 * @discussion For each item, scalar value is of unsigned intege type.
 */
@property (nonatomic, readonly, nullable) NSArray<NSNumber*> *LLAPTScenarios DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


#pragma mark - LLAPT Scenario Group Setting

@property (nonatomic, readonly, nullable) NSNumber *LLAPTGroupsNumber DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *scenarios DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

#pragma mark - APT NR
/**
 * Return whether the APT NR is on of this peripheral last cached.
 *
 * @discussion Scalar value is of BOOL type.
 */
@property (nonatomic, readonly, nullable) NSNumber *APTNRState DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

#pragma mark - APT EQ
/**
 * Return the count of APT EQ entry of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *APTEQCount DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the index of APT EQ entry current used of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *APTEQIndex DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings using for APT in Left side bud.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (nonatomic, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *APTEQSettingsOfLeft DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings using for APT in Right side bud.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (nonatomic, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *APTEQSettingsOfRight DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return a list of RTKBBproEQSetting objects which representing EQ settings using for APT in both bud.
 *
 * @discussion The returned RTKBBproEQSetting object may not be fully determine the parameter. Call -getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler: to make it full-fledged.
 */
@property (nonatomic, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *APTEQSettings DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


#pragma mark - APT Volume And Brightness

@property (nonatomic, readonly, nullable) NSNumber *syncAPTVolume DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the maximum Main APT volume level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *maxMainAPTVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the maximum Sub APT volume level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *maxSubAPTVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Main APT volume level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *leftMainAPTVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Sub APT volume level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *leftSubAPTVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Main APT volume level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *rightMainAPTVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Sub APT volume level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *rightSubAPTVolumeLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the maximum Main APT Brightness level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *maxMainBrightnessLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the maximum Sub APT Brightness level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *maxSubBrightnessLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Main APT Brightness level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *leftMainBrightnessLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Sub APT Brightness level of the left bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *leftSubBrightnessLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Main APT Brightness level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *rightMainBrightnessLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Return the current Sub APT Brightness level of the right bud last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *rightSubBrightnessLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


@property (nonatomic, readonly, nullable) NSNumber *APTVolumeOutLevel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


#pragma mark - APT Power On Delay Time

/**
 * Return whether the APT NR is on of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. Unit is second.
 */
@property (nonatomic, readonly, nullable) NSNumber *APTDelayTime DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");


#pragma mark - BudInfo

@property (nonatomic, readonly, nullable) NSNumber *budType DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
@property (nonatomic, readonly, nullable) NSNumber *primaryBudSide DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
@property (nonatomic, readonly, nullable) NSNumber *LRChannel DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
@property (nonatomic, readonly, nullable) NSNumber *singleOrLeftBattery DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
@property (nonatomic, readonly, nullable) NSNumber *rightBattery DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

- (void)connectProfile:(RTKBBproLegacyProfile)profile withCompletion:(nullable RTKLECompletionBlock)handler DEPRECATED_MSG_ATTRIBUTE("Use -connectProfile:withCompletionHandler: instead.");

- (void)cancelProfileConnection:(RTKBBproLegacyProfile)profile withCompletion:(nullable RTKLECompletionBlock)handler DEPRECATED_MSG_ATTRIBUTE("Use -cancelProfileConnection:withCompletionHandler: instead.");


@end

NS_ASSUME_NONNULL_END
