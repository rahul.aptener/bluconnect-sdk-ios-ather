//
//  RTKACEQRoutine.h
//  RTKLEFoundation
//
//  Created by jerome_gu on 2020/3/26.
//  Copyright © 2020 jerome_gu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RTKAudioConnectSDK/RTKACRoutine.h>
#import <RTKAudioConnectSDK/RTKBBproType.h>
#import <RTKAudioConnectSDK/RTKACBasicRoutine.h>
#import <RTKAudioConnectSDK/RTKACAudioRoutine.h>
#import <RTKAudioConnectSDK/RTKBBproEQSetting.h>
#import <RTKAudioConnectSDK/RTKBBproEQSettingPlaceholder.h>


NS_ASSUME_NONNULL_BEGIN

@class RTKACEQRoutine;

/// Methods an ``RTKACEQRoutine`` calls to report EQ related events on its delegate.
@protocol RTKACEQRoutineStateReporting <NSObject>
@optional

/// Tells the delegate that remote device request to change EQ index.
///
/// - Parameter routine: The routine that did request change EQ index.
/// - Parameter index: The new index requested to change to.
/// - Parameter mode: The mode which EQ index is in.
///
/// When this method is called, action should be start to make the real EQ index changing. SDK will perform actual index changing process automatically, so delegate can leave it out.
- (void)BBproEQRoutine:(RTKACEQRoutine *)routine didRequestChangeEQIndexTo:(NSUInteger)index ofEQMode:(RTKBBproEQMode)mode;


/// Tells the delegate that remote device did change EQ index.
///
/// - Parameter routine: The routine that report EQ index change.
/// - Parameter index: The EQ index current used.
- (void)BBproEQRoutine:(RTKACEQRoutine *)routine didReceiveLegacyEQIndexChangeTo:(NSUInteger)index;


/// Tells the delegate that a EQ index of a specified mode did change of a RTKACEQRoutine instance.
///
/// - Parameter routine: The routine that report eq index change.
/// - Parameter index: The eq index which is updated to.
/// - Parameter mode: The EQ mode peripheral is using.
- (void)BBproEQRoutine:(RTKACEQRoutine *)routine didReceiveEQIndexChange:(NSUInteger)index ofEQMode:(RTKBBproEQMode)mode;


/// Tells the delegate that a APT EQ index did change of a RTKACEQRoutine instance.
///
/// - Parameter routine: The routine that report APT eq index change.
/// - Parameter index: The eq index which is updated to.
- (void)BBproEQRoutine:(RTKACEQRoutine *)routine didReceiveAPTEQIndexChange:(NSUInteger)index;


/// Tells the delegate that remote device request to synchronize EQ parameters.
///
/// - Parameter routine: The routine that report eq index change.
/// - Parameter index: The eq index which is updated to.
/// - Parameter mode: The EQ mode peripheral is using.
- (void)BBproEQRoutine:(RTKACEQRoutine *)routine didRequestSynchronizeEQofIndex:(NSUInteger)index andMode:(RTKBBproEQMode)mode;

@end


/// An concrete routine which provides functionality to access EQ feature.
@interface RTKACEQRoutine : RTKACRoutine

/// The basic routine this routine uses.
@property RTKACBasicRoutine *basicRoutine;

/// The audio routine this routine uses.
@property RTKACAudioRoutine *audioRoutine;

/// The delegate object that receives events.
@property (nonatomic, weak) id <RTKACEQRoutineStateReporting> delegate;


/// Get the count of EQ setting entrys of all modes.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully. When succeed, a count of the specified EQ mode and type is received .(Deprecate in EQ2.0)
- (void)getEQEntryCountOfMode:(RTKBBproEQMode)mode
                         type:(RTKBBproSWEQType)type
        withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger count))handler;


#pragma mark - SPK EQ

/// Get the index of EQ setting entry current used of the specified mode.
///
/// - Parameter mode: The specified EQ mode.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
- (void)getCurrentEQIndexOfMode:(RTKBBproEQMode)mode
          withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger idx))handler;


/// Set the index of EQ setting entry current used of the specified mode.
///
/// - Parameter idx: The specified EQ index.
/// - Parameter mode: The specified EQ mode.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
- (void)setCurrentEQIndex:(NSUInteger)idx
                   ofMode:(RTKBBproEQMode)mode
    withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/// Get the parameter data of a EQ setting entry specified by index of the specified mode.
///
/// - Parameter idx: The specified EQ index.
/// - Parameter mode: The specified EQ mode.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully. When succeed, a paraData is received along with sample rate. (Deprecate in EQ2.0)
- (void)getEQParameterOfIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                   withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproSampleRate sampleRate, NSData *_Nullable paraData))handler;


/// Send the parameter data to a EQ setting entry specified by index of the specified mode.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully. (Deprecate in EQ2.0)
- (void)setEQParameterOfIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                     withData:(NSData *)data
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
            withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/// Get the parameter data of a EQ setting entry specified by index of the specified mode and use the parameter data to make the RTKBBproEQSetting object full-fledged.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully. (Deprecate in EQ2.0)
- (void)getEQParameterAtIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
           toResolveEQSetting:(RTKBBproEQSetting *)setting
            withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/// Set a EQ setting entry specified by index of the specified mode with a RTKBBproEQSetting object.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully. (Deprecate in EQ2.0)
- (void)setEQParameterAtIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                  withSetting:(RTKBBproEQSetting *)setting
            withCompletionHandler:(nullable RTKLECompletionBlock)handler;


#pragma mark - Legacy EQ setting


/// Get EQ Setting index of peripheral
///
/// - Parameter handler: This block is called upon completion. If the action take effect then success is YES and error is nil, and parameter currentIndex is the the EQ Setting Index current used in Peripheral, supportedIndexes is a bit field which indicate supported Indexes. Otherwise success is NO with an error.
- (void)getLegacyCurrentEQIndexStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproEQIndex currentIndex, RTKBBproEQIndex supportedIndexes))handler;


/// Set current used EQ Setting of peripheral
///
/// - Parameter index: A bitmask whose EQ Setting to use bit set 1.
- (void)setLegacyCurrentEQIndexTo:(RTKBBproEQIndex)index withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Get DSP version information of BBpro Peripheral.
///
/// - Parameter handler: Called upon completion. If success, info is a dictionary containning DSP Info element (a example next), else a error is provided. info Dictionary example:  @{@"Scenario": @(0x00), @"SF": @(0x03), @"ROM": @(0x01010101), @"RAM": @(0x01010101), @"Patch": @(0x01010101), @"SDK": @(0x01010101)}
///
/// Deprecated by more recent device.
- (void)getDSPInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSDictionary <NSString*,NSNumber*>*_Nullable info))handler;


/// Set DSP EQ effect parameter.
///
/// - Parameter paramterData: EQ raw data to send to DSP. The data should be return by call -[RTKBBproEQSetting parameterDataOfSampleRate:] method.
/// - Parameter handler: This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
///
/// This method set equalizer of 44.1kHz sample rate audio. Deprecated by more recent device.
- (void)setDSPEQ:(NSData *)paramterData withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Set DSP EQ effect parameter.
///
/// - Parameter paramterData: EQ raw data to send to DSP. The data should be return by call -[RTKBBproEQSetting parameterDataOfSampleRate:] method.
/// - Parameter sampleRate: The sample rate to which this EQ affect.
/// - Parameter handler: This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
///
/// Deprecated by more recent device.
- (void)setDSPEQData:(NSData *)paramterData ofSampleRate:(RTKBBproSampleRate)sampleRate withCompletionHandler:(nullable void(^)(BOOL, NSError*_Nullable))handler;


/// Clear DSP equalizer effect.
///
/// - Parameter handler: This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
///
/// Deprecated by more recent device.
- (void)clearDSPEQWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - APT EQ

/// Get current APT EQ index of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `APTEQIndex` when succeed.
- (void)getCurrentAPTEQIndexWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger APTEQIndex))handler;


/// Set current APT EQ index of this peripheral.
///
/// - Parameter index: The specified APT EQ index.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update `APTEQIndex` when succeed.
- (void)setCurrentAPTEQIndex:(NSUInteger)index withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Get the specified APT EQ parameters of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the specified APTEQSettings when succeed. (Deprecate in EQ2.0)
- (void)getEQParameterOfIndex:(NSUInteger)index andType:(RTKBBproSWEQType)type inMode:(RTKBBproEQMode)mode withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproSampleRate sampleRate, NSData *paraData))handler;


/// Set the specified APT EQ parameters of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the specified APTEQSettings(left/right) when succeed. (Deprecate in EQ2.0)
- (void)setEQParameterOfIndex:(NSUInteger)index
                      andType:(RTKBBproSWEQType)type
                       inMode:(RTKBBproEQMode)mode
                     withData:(NSData *)data
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                       forBud:(RTKEQBudSide)bud
            withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Get the specified APT EQ parameters of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the specified APTEQSettings when succeed. (Deprecate in EQ2.0)
- (void)getEQParameterAtIndex:(NSUInteger)idx ofType:(RTKBBproSWEQType)type inMode:(RTKBBproEQMode)mode toResolveEQSettings:(NSArray <RTKBBproEQSetting*> *)settings withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Set the specified APT EQ parameters of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the specified APTEQSettings(left/right) when succeed. (Deprecate in EQ2.0)
- (void)setEQParameterAtIndex:(NSUInteger)idx
                       ofType:(RTKBBproSWEQType)type
                       inMode:(RTKBBproEQMode)mode
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                  withSetting:(RTKBBproEQSetting *)setting
                       forBud:(RTKEQBudSide)bud
            withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - EQ (SPEC 2.0)

/// Get EQ info(count, index mapping, sampleRate) of this peripheral.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the `numberOfSavedSPKEQ`, `numberOfSavedMICEQ`, `normalEQIndexArray`, `gamingEQIndexArray`, `ancEQIndexArray`, `SPKEQSettings`, `currentNormalEQIndex`, `currentGamingEQIndex`, `currentANCEQIndex`, `currentSampleRate`, `supportedSampleRate`, `currentSPKEQMode`, `currentMICEQMode`, `APTEQSettings`, `APTEQSettingsOfLeft` or `APTEQSettingsOfRight` when succeed.
- (void)getEQInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Reset the specified EQ to the factory setting.
///
/// - Parameter type: The specified EQ type.
/// - Parameter mode: The specified EQ mode.
/// - Parameter index: The specified EQ mode index.
/// - Parameter side: The specified bud side.
/// - Parameter setting: The current EQ setting.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the `SPKEQSettings`, `APTEQSettings`, `APTEQSettingsOfLeft` or `APTEQSettingsOfRight` when succeed.
- (void)resetEQOfType:(RTKBBproSWEQType)type
                 mode:(RTKBBproEQMode)mode
                index:(uint8_t)index
              andSide:(RTKEQBudSide)side
   toResolveEQSetting:(RTKBBproEQSetting *)setting
withCompletionHandler:(nullable void(^)(BOOL success, NSError * _Nullable error, RTKBBproSampleRate sampleRate, NSData * _Nullable paraData))handler;


/// Set the specified EQ parameters.
///
/// - Parameter applyOrSave: Specifies whether the current EQ setting takes effect immediately or needs to be saved on the device.
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the `SPKEQSettings`, `APTEQSettings`, `APTEQSettingsOfLeft` or `APTEQSettingsOfRight` when succeed.
- (void)setEQParameterAtIndex:(NSUInteger)idx
                       ofType:(RTKBBproSWEQType)type
                       mode:(RTKBBproEQMode)mode
                 sampleRate:(RTKBBproSampleRate)sampleRate
                  withSetting:(RTKBBproEQSetting *)setting
                       forBud:(RTKEQBudSide)bud
                  applyOrSave:(RTKApplyOrSaveEQ)applyOrSave
        withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/// Get the specified EQ parameters.
///
/// - Parameter handler: A block to called when this task complete successfullly or unsuccessfully.
///
/// Will update the `SPKEQSettings`, `APTEQSettings`, `APTEQSettingsOfLeft` or `APTEQSettingsOfRight` when succeed.
- (void)getEQParameterAtIndex:(NSUInteger)idx
                       ofType:(RTKBBproSWEQType)type
                         mode:(RTKBBproEQMode)mode
                          bud:(RTKEQBudSide)bud
           toResolveEQSetting:(RTKBBproEQSetting *)setting
        withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end



@interface RTKACEQRoutine (Cache)

/// Return cached current EQ index used for a legacy SOC implementation.
///
/// Affected by ``RTKACEQRoutine/getLegacyCurrentEQIndexStateWithCompletionHandler:`` and ``RTKACEQRoutine/setLegacyCurrentEQIndexTo:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *legacyCurrentEQIndex;


/// Return cached supported EQ indexes used for a legacy SOC implementation.
///
/// Affected by ``RTKACEQRoutine/getLegacyCurrentEQIndexStateWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *legacySupportedEQIndexes;


/// Return an ``RTKBBproEQSetting`` instance which currently used of a legacy implementation.
@property (readonly, nullable) RTKBBproEQSetting *legacyEQSettingInUse;


/// Return a list of ``RTKBBproEQSetting`` objects which representing EQ settings in remote device runing normal mode.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler:`` with `RTKBBproEQMode_normal`. The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler:`` to make it full-fledged. (Deprecate in EQ2.0)
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *normalModeEQSettings;


/// Return an ``RTKBBproEQSetting`` instance which currently used for normal mode.
///
/// Affected by ``RTKACEQRoutine/getCurrentEQIndexOfMode:withCompletionHandler:`` and ``RTKACEQRoutine/setCurrentEQIndex:ofMode:withCompletionHandler:`` with  `RTKBBproEQMode_normal` . (Deprecate in EQ2.0)
@property (readonly, nullable) RTKBBproEQSetting *normalModeEQSettingInUse;


/// Return a list of  ``RTKBBproEQSetting`` objects which representing EQ settings in remote device excuting gaming mode.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler:`` with `RTKBBproEQMode_gaming` . The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler:`` to make it full-fledged. (Deprecate in EQ2.0)
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *gamingModeEQSettings;


/// Return the current ``RTKBBproEQSetting`` used of the remote device in gaming mode.
///
/// Affected by ``RTKACEQRoutine/getCurrentEQIndexOfMode:withCompletionHandler:`` and ``RTKACEQRoutine/setCurrentEQIndex:ofMode:withCompletionHandler:`` with  `RTKBBproEQMode_gaming` . (Deprecate in EQ2.0)
@property (readonly, nullable) RTKBBproEQSetting *gamingModeEQSettingInUse;


/// Return a list of ``RTKBBproEQSetting`` objects which representing EQ settings in remote device runing ANC mode.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler:`` with `RTKBBproEQMode_anc` . The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterOfIndex:ofMode:toResolveEQSetting:completionHandler:`` to make it full-fledged. (Deprecate in EQ2.0)
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *ANCModeEQSettings;


/// Return an ``RTKBBproEQSetting`` instance which currently used for ANC mode.
///
/// Affected by ``RTKACEQRoutine/getCurrentEQIndexOfMode:withCompletionHandler:`` and ``RTKACEQRoutine/setCurrentEQIndex:ofMode:withCompletionHandler:``  with `RTKBBproEQMode_anc` . (Deprecate in EQ2.0)
@property (readonly, nullable) RTKBBproEQSetting *ANCModeEQSettingInUse;


/// Return the count of APT EQ entry of this peripheral last cached.
///
/// Scalar value is of unsigned integer type. Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler`` or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *APTEQCount;


/// Return the index of APT EQ entry current used.
///
/// Scalar value is of unsigned integer type. Affected by ``RTKACEQRoutine/getCurrentAPTEQIndexWithCompletion:``,  ``RTKACEQRoutine/setCurrentAPTEQIndex:completion:`` and ``RTKACEQRoutine /getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *APTEQIndex;


/// Return a list of ``RTKBBproEQSetting`` objects which representing EQ settings of left bud in remote device runing APT mode.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler`` or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``. The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler:`` or ``RTKACEQRoutine/getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler:`` or to make it full-fledged.
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *APTEQSettingsOfLeft;


/// Return a list of ``RTKBBproEQSetting`` objects which representing EQ settings of right bud in remote device runing APT mode.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler`` or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``. The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler:`` or ``RTKACEQRoutine/getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler:`` to make it full-fledged.
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *APTEQSettingsOfRight;


/// Return a list of ``RTKBBproEQSetting`` objects which representing EQ settings of the both buds in remote device runing APT mode.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler`` or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``. The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterAtIndex:ofType:inMode:toResolveEQSetting:completionHandler:`` or ``RTKACEQRoutine/getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler:`` to make it full-fledged.
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *APTEQSettings;


#pragma mark - EQ2.0

/// Return the number of the SPKEQ which could be saved in SOC.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *numberOfSavedSPKEQ;


/// Return the number of the MICEQ which could be saved in SOC.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *numberOfSavedMICEQ;


/// Return an index array representing the position of normal EQ in SPKEQSettings.
///
 /// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *normalEQIndexArray;


/// Return an index array representing the position of gaming EQ in SPKEQSettings.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *gamingEQIndexArray;


/// Return an index array representing the position of anc EQ in SPKEQSettings.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *ancEQIndexArray;


/// Return a list of ``RTKBBproEQSetting`` objects which representing SPKEQ settings in remote device.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``. The returned `RTKBBproEQSetting` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler:`` to make it full-fledged.
@property (readonly, nullable) NSArray <RTKBBproEQSetting*> *SPKEQSettings;


/// Return a list of ``RTKBBproEQSettingPlaceholder`` objects which representing SPKEQ settings of left bud. (Peripheral supports RTKBBproCapabilityType_EQAdjustSeparately)
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``. The returned `RTKBBproEQSettingPlaceholder` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler:`` to make it full-fledged.
@property (readonly, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *SPKEQSettingsOfLeft;


/// Return a list of ``RTKBBproEQSettingPlaceholder`` objects which representing SPKEQ settings of right bud. (Peripheral supports RTKBBproCapabilityType_EQAdjustSeparately)
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``. The returned `RTKBBproEQSettingPlaceholder` object may not be fully determine the parameter. Call ``RTKACEQRoutine/getEQParameterAtIndex:ofType:mode:bud:toResolveEQSetting:withCompletionHandler:`` to make it full-fledged.
@property (readonly, nullable) NSArray <RTKBBproEQSettingPlaceholder*> *SPKEQSettingsOfRight;


/// Return the current normal EQ index.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:`` and ``RTKACEQRoutine/setCurrentEQIndex:ofMode:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *currentNormalEQIndex;


/// Return the current gaming EQ index.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:`` and  ``RTKACEQRoutine/setCurrentEQIndex:ofMode:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *currentGamingEQIndex;


/// Return the current anc EQ index.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:`` and  ``RTKACEQRoutine/setCurrentEQIndex:ofMode:withCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *currentANCEQIndex;


/// Return the current sample rate.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *currentSampleRate;


/// Return a list of the sample rates the peripheral supported.
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSArray<NSNumber *> *supportedSampleRate;


/// Return the current SPKEQ mode (normal, gaming or anc).
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *currentSPKEQMode;


/// Return the current MICEQ mode (apt).
///
/// Affected by ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (nonatomic, readonly, nullable) NSNumber *currentMICEQMode;


/// Return the voice EQ setting.
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler:`` or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (readonly, nullable) RTKBBproEQSettingPlaceholder *VoiceEQSetting;


/// Return the voice EQ setting of left bud. (Peripheral supports RTKBBproCapabilityType_EQAdjustSeparately)
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler:``  or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (readonly, nullable) RTKBBproEQSettingPlaceholder *VoiceEQSettingOfLeft;


/// Return the voice EQ setting of right bud. (Peripheral supports RTKBBproCapabilityType_EQAdjustSeparately)
///
/// Affected by ``RTKACEQRoutine/getEQEntryCountOfMode:type:withCompletionHandler:``  or ``RTKACEQRoutine/getEQInfoWithCompletionHandler:``.
@property (readonly, nullable) RTKBBproEQSettingPlaceholder *VoiceEQSettingOfRight;

@end

NS_ASSUME_NONNULL_END
