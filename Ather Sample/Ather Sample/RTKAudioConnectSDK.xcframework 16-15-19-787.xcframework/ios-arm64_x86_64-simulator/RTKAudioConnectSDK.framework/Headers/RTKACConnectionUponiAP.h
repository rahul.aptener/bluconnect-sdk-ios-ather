//
//  RTKACConnectionUponiAP.h
//  RTKBTFoundation
//
//  Created by jerome_gu on 2020/3/4.
//  Copyright © 2020 jerome_gu. All rights reserved.
//

#import <RTKLEFoundation/RTKLEFoundation.h>
#import <RTKAudioConnectSDK/RTKACRoutineContainer.h>

NS_ASSUME_NONNULL_BEGIN

/// A iAP connection with an Audio Connect feature implemented device.
///
/// An ``RTKACConnectionManager`` creates an instance of this class if it discovers a connection with *Audio Connect* device using iAP.
@interface RTKACConnectionUponiAP : RTKConnectionUponiAP <RTKACRoutineContainer>

@end

NS_ASSUME_NONNULL_END
