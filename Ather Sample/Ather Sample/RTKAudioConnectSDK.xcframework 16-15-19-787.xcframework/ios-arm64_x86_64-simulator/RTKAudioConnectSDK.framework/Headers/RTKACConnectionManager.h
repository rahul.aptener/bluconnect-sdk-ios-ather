//
//  RTKACConnectionManager.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2021/10/18.
//

#import <RTKLEFoundation/RTKLEFoundation.h>

NS_ASSUME_NONNULL_BEGIN

/// A connection manager which manages connections with Audio Connect devices.
///
/// You create an instance of this class, use it to discover connections with *Audio Connect* devices.
@interface RTKACConnectionManager : RTKProfileConnectionManager

@end

NS_ASSUME_NONNULL_END
