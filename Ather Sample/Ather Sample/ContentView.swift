//
//  ContentView.swift
//  Ather Sample
//
//  Created by Rahul Gaur on 11/07/24.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @EnvironmentObject var vm:MainViewModel
    @FocusState private var isFocused: Bool

    var body: some View {
        
        ScrollView(.vertical){
            VStack(spacing:12, content: {
                
                Text( vm.connected ? "Connected" : "Disconnected" )
                    .foregroundColor(.white)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .frame(maxWidth: .infinity)
                    .frame(height: 70)
                    .background(vm.connected ? Color.green : Color.red )
                    .cornerRadius(6)
                    .padding(.bottom, 20)
                
                Text( "Model: \(vm.model.label)" )
                    .foregroundColor(.white)
                    .fontWeight(.semibold)
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 40)
                    .background( Color.black )
                
                if vm.model == .HALOBit {
                    
                    Text( "Version: \(vm.haloBitFwVersion)" )
                        .foregroundColor(.white)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 40)
                        .background( Color.black )

                    
                    Text( "Battery: \(vm.haloBitBattery)" )
                        .foregroundColor(.white)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 40)
                        .background( Color.black )

                    
                }
                
                Toggle("Chit Chat", isOn: Binding(get: {
                    vm.chitChat
                }, set: { v in
                    vm.toggleChitChat(enable: v)
                }))
                    .disabled(!vm.connected)
                
                Toggle("Music Sharing", isOn: $vm.musicSharing)
                    .disabled(!vm.connected)
                
                if vm.chitChat {
                    Text( "Active Group: \(vm.chitChatGroup?.name ?? "")" )
                        .foregroundColor(.black.opacity(0.7))
                        .fontWeight(.medium)
                        .frame(maxWidth: .infinity,alignment: .leading)
                    
                    TextEditor(text:$vm.activeChitChatGroupData )
                        .font(.caption.weight(.regular))
                        .frame(maxWidth: .infinity,alignment: .leading)
                        .clipShape(RoundedRectangle(cornerRadius:4))
                        .focused($isFocused)
                    
                    Button(action: {
                        self.isFocused = false
                        vm.createRandomGroup()
                    }){
                        Text( "New Private Group" )
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame( height: 50)
                            .padding(.horizontal, 40)
                            .background( Color.black )
                            .cornerRadius(38)
                    }
                    
                    Spacer()
                    
                    TextEditor(text:$vm.newChitChatGroupData )
                        .font(.caption.weight(.regular))
                        .frame(maxWidth: .infinity,alignment: .leading)
                        .frame(minHeight: 50)
                        .clipShape(RoundedRectangle(cornerRadius:4))
                        .focused($isFocused)
                    
                    Button(action: {
                        self.isFocused = false
                        vm.joinGroup(invitationData: vm.newChitChatGroupData)
                    }){
                        Text( "Join Private Group" )
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame( height: 50)
                            .padding(.horizontal, 40)
                            .background( Color.black )
                            .cornerRadius(38)
                    }
                }
                
                if vm.otaProgress > 0{
                    Text( "OTA PROGRESS: \(vm.otaProgress)" )
                        .foregroundColor(.white)
                        .fontWeight(.semibold)
                        .multilineTextAlignment(.center)
                        .frame( height: 50)
                        .padding(.horizontal, 40)
                        .background( Color.black )
                        .cornerRadius(38)
                }
                
//                if vm.otaAvailable {
                    Button(action: {
                        vm.startOta()
                    }){
                        Text( "Start OTA" )
                            .foregroundColor(.white)
                            .fontWeight(.semibold)
                            .multilineTextAlignment(.center)
                            .frame( height: 50)
                            .padding(.horizontal, 40)
                            .background( Color.black )
                            .cornerRadius(38)
                    }
//                }
                
            })
            .padding(40)
            .frame(maxWidth: .infinity,maxHeight: .infinity)
            .background(Color.black.opacity(0.06))
            .cornerRadius(38)
            .padding(20)
    //        .shadow(color: .black, radius: 4)
        }
    }


}

#Preview {
    ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
}
