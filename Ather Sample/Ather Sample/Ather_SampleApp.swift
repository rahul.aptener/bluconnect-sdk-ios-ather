//
//  Ather_SampleApp.swift
//  Ather Sample
//
//  Created by Rahul Gaur on 11/07/24.
//

import SwiftUI
import BluConnect

@main
struct Ather_SampleApp: App {
    @ObservedObject private var vm:MainViewModel = MainViewModel()
    let persistenceController = PersistenceController.shared
    
    var body: some Scene {
        
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(vm)
        }
        
    }
}

class MainViewModel: BluConnectService, ObservableObject{
    
    init(){
        super.init()
    }
    
    @Published public var connected:Bool = false
    @Published public var model:BluArmorModel = .NO_DEVICE
    @Published public var otaAvailable:Bool = false
    @Published public var otaProgress:Int = 0
    
    @Published public var haloBitBattery:Int = 0
    @Published public var haloBitFwVersion:String = ""
    
    func setHaloOtaPath(){
        if let path = Bundle.main.path(forResource: "halo_ota_20aug", ofType: "bin") {
            do {
                super.setSideLoadOtaBinaries(OtaBinaryPath.OtaURL(url: URL(fileURLWithPath: path)))
                print("reading data: ")
            } catch {
                print("Error reading data: \(error)")
            }
        }
    }
    func setHaloBitOtaPath(){
        if let path = Bundle.main.path(forResource: "halo_bit_ota", ofType: "bin") {
            do {
                super.setSideLoadOtaBinaries(OtaBinaryPath.OtaURL(url: URL(fileURLWithPath: path)))
                print("reading data: ")
            } catch {
                print("Error reading data: \(error)")
            }
        }
    }
    
    func startOta(){
        (getActiveDevice() as? HALO)?.otaSwitch.switchToOtaMode()
    }
    
    @Published public var chitChat = true
    
    @Published public var musicSharing = true {
        didSet{
            (getActiveDevice() as? HALO)?.rideGrid.toggleMusicSharing(enable: musicSharing)
        }
    }
    
    @Published public var chitChatState:RideGridState = .OFF
    @Published public var chitChatGroup:RideGridGroup? = nil {
        didSet{
            if let ridegridGroup = self.chitChatGroup, ridegridGroup.privateGroup {
                self.activeChitChatGroupData = getInvitationData(ridegridGroup: ridegridGroup)
            }
            
        }
    }
    
    @Published public var activeChitChatGroupData:String = ""
    @Published public var newChitChatGroupData:String = ""
    
    override func onConnectionStateChange(state: ConnectionState) {
        print("onConnectionStateChange: \(state)")

        switch state {
            
        case .DeviceReady(let device):
            
            self.model = device.model
            
            print("DeviceReady: \(device.model.label)  \(device.deviceName)")
            
            if device.model == .HALO_OTA{
                
                setHaloOtaPath()
                (getActiveDevice() as? S20XOta)?.firmwareUpdate.observe(onUpdate: { value in
                    switch value{
                    case .Upgrading(appProgress: let appProgress, ridegridProgress: let ridegridProgress):
                        self.otaProgress = appProgress
                    default:
                        self.otaProgress = 0
                    }
                })
//            } else if device.model == .HALOBit_OTA{
//                
//                setHaloBitOtaPath()
//                (getActiveDevice() as? HALOBitOta)?.firmwareUpdate.observe(onUpdate: { value in
//                    switch value{
//                    case .Upgrading(appProgress: let appProgress, ridegridProgress: let ridegridProgress):
//                        self.otaProgress = appProgress
//                    default:
//                        self.otaProgress = 0
//                    }
//                })
            }else if device.model == .HALOBit{
//                DispatchQueue.main.asyncAfter(deadline: .now() + 2){
//                    (self.getActiveDevice() as? HALOBit)?.reboot()
//                }
                
                
                setHaloBitOtaPath()
                self.otaProgress = 0
                
                self.connected = true
                
                (getActiveDevice() as? HALOBit)?.deviceInfoHaloBit.observe(onUpdate: { value in
                    print("VERSION: \(value?.firmwareApplicationVersion)")
                    self.haloBitFwVersion = value?.firmwareApplicationVersion ?? self.haloBitFwVersion
                })
                
                (getActiveDevice() as? HALOBit)?.battery.observe(onUpdate: { value in
                    print("BATTERY: \(value.level)")
                    self.haloBitBattery = value.level
                })
                
//                (getActiveDevice() as? HALOBit)?.rideGridLite.observe(onUpdate: self.rideGridStateObserver)

                (getActiveDevice() as? HALOBit)?.rideGridLite.observe(onUpdate: { value in
                    print("RG_STATE: \(value)")
                    self.rideGridStateObserver(rideGridState: value)
                })

                
//                self.chitChatGroup = (getActiveDevice() as? HALOBit)?.rideGrid.getActiveGroup()
//                (getActiveDevice() as? HALOBit)?.otaSwitch.observe(onUpdate: { value in
//                    print("OTA STATE \(value)")
//                    switch value{
//                    case .NoUpdateAvailable:
//                        self.otaAvailable = false
//                    case .UpdateAvailable(changeLogs: let changeLogs):
//                        self.otaAvailable = true
//                    @unknown default:
//                        self.otaAvailable = false
//                    }
//                })
                
            }else{
                
                setHaloOtaPath()
                self.otaProgress = 0
                
                self.connected = true
                self.chitChatGroup = (getActiveDevice() as? HALO)?.rideGrid.getActiveGroup()
                
                (getActiveDevice() as? HALO)?.rideGrid.observe(onUpdate: self.rideGridStateObserver)
                
                (getActiveDevice() as? HALO)?.deviceInfo.observe(onUpdate: { value in
                    print("DEVICE INFO: \(value)")
                })
                
                (getActiveDevice() as? HALO)?.otaSwitch.observe(onUpdate: { value in
                    print("OTA STATE \(value)")
                    switch value{
                    case .NoUpdateAvailable:
                        self.setHaloOtaPath()
                        self.otaAvailable = false
                    case .UpdateAvailable(changeLogs: let changeLogs):
                        self.otaAvailable = true
                    @unknown default:
                        self.otaAvailable = false
                    }
                })
            }
            break
        default:
            self.connected = false
            self.model = .NO_DEVICE
            self.haloBitFwVersion = ""
            self.haloBitBattery = 0
            break
        }
    }
    
    func rideGridStateObserver(rideGridState:RideGridState){
        
        print("rideGridStateObserver: \(rideGridState)")

        self.chitChatState = rideGridState

        self.chitChat = {
            switch rideGridState{
            case .DISCOVERY, .CONNECTED, .ACTIVE_CALL, .MUTED:
                return true
            default:
                return false
            }
        }()
    }
    
    func createRandomGroup(){
        self.chitChatGroup = (getActiveDevice() as? HALO)?.rideGrid.create(ridegridGroupName: ridingGroupNames.randomElement()!, isPrimary: true)
    }
    
    func getInvitationData(ridegridGroup:RideGridGroup) -> String {
        return (getActiveDevice() as? HALO)?.rideGrid.encodeToQrCodeData(ridegridGroup: ridegridGroup) ?? "ERROR !"
    }
    
    
    func joinGroup(invitationData:String) {
        guard let invitation = RideGridGroupInvitation.decodeFromQrCodeData(qrCodeData: invitationData) else {return}
        
        if model == .HALOBit {
            let _ = (getActiveDevice() as? HALOBit)?.rideGridLite.join(rideGridGroupInvitation: invitation, isPrimary: true)
            print("HALOBit: Joined \(invitation.ridegridGroup.name)")
        } else {
            let _ = (getActiveDevice() as? HALO)?.rideGrid.join(rideGridGroupInvitation: invitation, isPrimary: true)
        }
        
        self.chitChatGroup = invitation.ridegridGroup
    }
    
    func toggleChitChat(enable:Bool){
        self.chitChat = enable
        if (connected){
            if model == .HALOBit {
                enable ? (getActiveDevice() as? HALOBit)?.rideGridLite.turnOn() : (getActiveDevice() as? HALOBit)?.rideGridLite.turnOff()
            } else {
                enable ? (getActiveDevice() as? HALO)?.rideGrid.turnOn() : (getActiveDevice() as? HALO)?.rideGrid.turnOff()
            }
        }
    }
    
    let ridingGroupNames = [
        "Thunder Riders",
        "Trail Blazers",
        "Iron Stallions",
        "Road Warriors",
        "Silver Serpents",
        "Midnight Riders",
        "Steel Thunder",
        "Wild Hogs",
        "Chrome Crusaders",
        "Rebel Riders",
        "Viper Vanguards",
        "Asphalt Avengers",
        "Highway Outlaws",
        "Phoenix Riders",
        "Shadow Stalkers",
        "Urban Nomads",
        "Ghost Riders",
        "Desert Wolves",
        "Night Ravens",
        "Freewheelers"
    ]
    
}

