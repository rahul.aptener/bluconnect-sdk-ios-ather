# BluConnect - BluArmor Companion SDK

BluConnect is an library designed for Bluetooth Low Energy  communication with BluArmor Devices. It allows for discovery, connecting and communicating with devices that are powered by BluArmor technology.

`BluConnectManager` exposes high level API for connecting and communicating with BluArmor devices.

## Importing

SDK is available under root path of this repository.

Add `BluConnect.xcframework` as a framework to the parent app via Xcode.

Then add the following packages
Swinject: https://github.com/Swinject/Swinject
Swift Protobuf: https://github.com/apple/swift-protobuf

SDK need several permissions to access Bluetooth, Location updates, Camera and background modes
### Add following to the Info.plist
* Privacy - Bluetooth Always Usage Description
* Privacy - Bluetooth Peripheral Usage Description
* Required background modes
    * App registers for location updates
    * App communicates using CoreBluetooth
    * App shares data using CoreBluetooth
    * App communicates with an accessory
    * App processes data in the background

#### Setting up

The framework supports 13.0 as minimum iOS version

### SDK prerequisites

Some features require addiitonal permission to work.

#### Location

SDK also needs to track the user’s location in the background to enable features such as RIDEGRID radar view and Dyanmic Volume controls based on Rider’s moving average speed.

#### Contact

SDK announces WhatsApp messages along with the sender name by looking up the sender phone number in the Contacts database. Contact access is also required to auto answer/reject phone calls.

## Usage

Extend `BluConnectService` with to access the range of SDK features. Sdk runs in background to auto connect and keep the BLE connection alive even when the phone is in standby mode.

The first step is to extend the `BluConnectService` and override `onConnectionStateChange(state: ConnectionState)` to access `ConnectionState`.

```swift
class ConnectionManager: BluConnectService {
    // This method will emit ConnectionState events 
    // To get the device we can use ConnectionState.device
    // On `ConnectionState.Ready` state, device is connected and ready to use
    override func onConnectionStateChange(state: ConnectionState) {
    }
}
```

If BluArmor device is found in the Bluetooth range (~10 meters), connection process will be initiated and the `ConnectionState` changes will be notified via  `onConnectionStateChange(...)` method call as the connection gets established.

```swift
override func onConnectionStateChange(state: ConnectionState) {
    switch state{
    case .Idle(reason: let reason):
        break // SDK is initializing or Bluetooth is not available
    case .Connecting(bluarmor: let bluarmor):
        break // Connection request initiated
    case .Connected(bluarmor: let bluarmor):
        break // Device is connected
    case .Failed(bluarmor: let bluarmor):
        break // Connection request failed
    case .Ready(bluarmor: let bluarmor):
        break // Device is connected and ready to use
    case .Disconnecting(bluarmor: let bluarmor):
        break // Disconnection request initiated
    case .Disconnected(bluarmor: let bluarmor):
        break // Device disconnected successfully
    case .Scanning(devices: let devices):
        break // Discovering nearby devices
    @unknown default:
        break // 
    }
}
```

Once [`onConnectionStateChange(...)`] emits `ConnectionState.Ready` state, it implies that the device is connected and ready to use. To get the connected `BluArmor` device use `ConnectionState.Ready.device` or `getActiveDevice()`.

## Supported BluArmor Devices

* **SX20**
  
### BluArmor Device Usage

BluArmor device exposes supported features as a class property.
For example `SX20` supports `Volume`, `RIDEGRID`, `RIDELYNK`, `Speed Dial`  and more...

To observe specific feature's state/value, use `observe` method.

```swift
// Once the connection state reflect device is ready
let device:SX20 = getActiveDevice()

private func mediaPlayerObserver(state:MediaState){ //update UI }
device.mediaPlayer.observe(onUpdate: self.mediaPlayerObserver(state:))

```

To interact with device's feature.

```swift
// Once the connection state reflect device is ready
let device:SX20 = getActiveDevice()
// Play the music
device.mediaPlayer.play()
```

## Firmware Update
BluArmor supports firmware upgrade over Bluetooth.

If firmware update is available, call `FirmwareVersion.startFirmwareUpdate()` method to start the firmware update process

```swift
// Once the connection state reflect device is ready
let device:SX20 = getActiveDevice()
// Function will return the State to confirm if update started of failed with reason
device.otaSwitch.switchToOtaMode()
```



## Best practices

#### Ask for Bluetooth permission upfront

Bluetooth permission is mandatory for BLE discovery.

## SDK Documentation

Follow the path: [docs/index.html](docs/)

## Test App

Compile the test app from BluArmor target with BluArmor.xcodeproj
