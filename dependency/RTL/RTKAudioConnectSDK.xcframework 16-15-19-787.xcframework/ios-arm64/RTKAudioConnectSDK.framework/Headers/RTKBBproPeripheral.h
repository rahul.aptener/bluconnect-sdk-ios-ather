//
//  RTKBBproPeripheral.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/4/11.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <RTKLEFoundation/RTKLEFoundation.h>

#import <RTKAudioConnectSDK/RTKBBproType.h>
#import <RTKAudioConnectSDK/RTKANCSNotificationEvent.h>
#import <RTKAudioConnectSDK/RTKANCSNotificationAttribute.h>
#import <RTKAudioConnectSDK/RTKBBproEQSetting.h>
#import <RTKAudioConnectSDK/RTKBBproEQSettingPlaceholder.h>
#import <RTKAudioConnectSDK/RTKBBproPeripheralStateCache.h>


NS_ASSUME_NONNULL_BEGIN


@class RTKBBproPeripheral;

// TODO: 定义更多的protocol方法，报告设备主动上报的state。
/**
 * A protocol that defines additional methods that RTKBBproPeripheral instances call on their delegates to handle peripheral events.
 */
@protocol RTKBBproPeripheralDelegate <RTKLEPeripheralDelegate>
@optional

/**
 * Tells the delegate that remote device did change current used language.
 *
 * @param peripheral The peripheral that did change the eq index.
 * @param lang The new language.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveLanguageChangeTo:(RTKBBproLanguageType)lang;

/**
 * Tells the delegate that remote device did change current used language.
 *
 * @param peripheral The peripheral that did change battery level.
 * @param primaryLvl Battery level of primary bud. @c RTKBBproBatteryLevelInvalid means this value is not valid.
 * @param secondaryLvl Battery level of secondary bud. @c RTKBBproBatteryLevelInvalid means this value is not valid.
 * @param cradleLvl Battery charge level of crade if exist. @c RTKBBproBatteryLevelInvalid means this value is not valid.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral
didReceiveUpdateOfPrimaryBatteryLevel:(RTKBBproBatteryLevel)primaryLvl
  secondaryBatteryLevel:(RTKBBproBatteryLevel)secondaryLvl
     cradleBatteryLevel:(RTKBBproBatteryLevel)cradleLvl;

/**
 * Tells the delegate that remote device did change current APT(Audio Pass Through) state.
 *
 * @param peripheral The peripheral that did change APT.
 * @param isOn Whether APT is on.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveChangeOfAPTState:(BOOL)isOn;

/**
 * Tells the delegate that remote device did change current APT(Audio Pass Through) state.
 *
 * @param peripheral The peripheral that did update RWS state.
 * @param isOn Whether RWS is on.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveChangeOfRWSState:(BOOL)isOn;

/**
 * Tells the delegate that remote device did change current RWS channel.
 *
 * @param peripheral The peripheral that did change channel.
 * @param direction The new direction.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveChangeOfRWSChannel:(RTKBBproChannelDirection)direction;

/**
 * Tells the delegate that remote device did change current ANC state.
 *
 * @param peripheral The peripheral that did change ANC.
 * @param isOn Whether ANC is on.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveChangeOfANCState:(BOOL)isOn;


/**
 * Tells the delegate that remote device did receive a ANCS notification.
 *
 * @param peripheral The peripheral that did receive event.
 * @param event The newly received notification.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveANCSNotification:(RTKANCSNotificationEvent *)event;


/**
 * Tells the delegate that remote device did receive a ANCS attribute.
 *
 * @param peripheral The peripheral that did receive attribute.
 * @param attribute The newly received attribute.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveANCSAttribute:(RTKANCSNotificationAttribute *)attribute;

/**
 * Tells the delegate that remote device did change low latency state.
 *
 * @param peripheral The peripheral that did change low latency state.
 * @param isOn Whether low latency is on.
 * @param lvl The low latency level.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveUpdateOfGamingMode:(BOOL)isOn lowLatencyLevel:(NSUInteger)lvl;

/**
 * Tells the delegate that remote device did update ANC state.
 *
 * @param peripheral The peripheral that did change ANC state.
 * @param isOn Whether ANC is on.
 * @param modes All ANC modes supported by this device.
 * @param idx The mode index current used.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveUpdateOfANCState:(BOOL)isOn ANCModes:(NSArray<NSNumber*>*)modes currentANCModeIndex:(NSUInteger)idx;

/**
 * Tells the delegate that remote device request to change EQ index.
 *
 * @param peripheral The peripheral that did request change EQ index.
 * @param index The new index requested to change to.
 * @param mode The mode which EQ index is in.
 *
 * @discussion When this method is called, action should be start to make the real EQ index changing. SDK will perform actual index changing process automatically, so delegate can leave it out.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didRequestChangeEQIndexTo:(NSUInteger)index ofEQMode:(RTKBBproEQMode)mode;


/**
 * Tells the delegate that remote device did change EQ index.
 *
 * @param peripheral The peripheral that did change EQ index.
 * @param index The EQ index current used.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveLegacyEQIndexChangeTo:(NSUInteger)index;


/**
 * Tells the delegate that a EQ index of a specified mode did change of a RTKBBproPeripheral instance.
 *
 * @param peripheral The peripheral that did change the eq index.
 * @param index The eq index which is updated to.
 * @param mode The EQ mode peripheral is using.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveEQIndexChange:(NSUInteger)index ofEQMode:(RTKBBproEQMode)mode;

/**
 * Tells the delegate that a APT EQ index did change of a RTKBBproPeripheral instance.
 *
 * @param peripheral The peripheral that did change the eq index.
 * @param index The eq index which is updated to.
 */
- (void)BBproPeripheral:(RTKBBproPeripheral *)peripheral didReceiveAPTEQIndexChange:(NSUInteger)index;


@end



/*!
 * Represents a remote BBpro-series Bluetooth LE device.
 *
 * @discussion This class defines methods for use to interact with a remote peripheral through BBpro GATT service. Thoese interactions contain geting a device state (i.e. getting battery level) and setting a new configuration (i.e. setting volume level). Almost all those methods execute asynchronously, when task this method start is completed, the completionHandler block get called.
 *
 * You should not call any methods before this peripheral is connected by @c RTKBBproProfile(or subclass) instance, @c interoperateAble property return a boolean value indicating whether those methods can be called.
 *
 * Not all features are supported by the connected peripheral, methods will report failure if device not support. Call @c -isAvailableFor: with feature constant to known if the feature is supported by this peripheral.
 *
 * @c RTKBBproPeripheral provides a @c cache property which save previously determined device state (e.g. volume level) temporarily. You get cached state by @c RTKBBproPeripheralStateCache instance properties. All most every property is KVO-applicable, which means that you can observing those properties to get notified when state updated.
 *
 * This class instance is not expected to be created manually. You get a RTKBBproPeripheral instance by RTKBBproProfile object methods.
 */
DEPRECATED_MSG_ATTRIBUTE("This class is only used for legacy compatibility. Use RTKACConnectionUponGATT class instead")
@interface RTKBBproPeripheral : RTKLEPeripheral <RTKPackageCommunicationClient> {
@protected
    CBCharacteristic *_readCharacteristic;
    RTKRequestCommunication *_communication;
    RTKLECompletionBlock _openResultBlock;
}

/**
 * The delegate object specified to receive peripheral events.
 */
@property (nonatomic, weak) id<RTKBBproPeripheralDelegate> delegate;

/**
 * Return a boolean value indicating whether high application commucation can be excuted.
 *
 * @discussion The property is KVO-capable.
 */
@property (nonatomic, readonly) BOOL interoperateAble;


/**
 *Return the command version supported by the SDK.
 */
@property (nonatomic, readonly, nullable) NSNumber *SDKCMDVersion;

/**
 *Return the EQ version supported by the SDK.
 */
@property (nonatomic, readonly, nullable) NSNumber *SDKEQVersion;

/**
 * Return the command version info of this device represented.
 *
 * @discussion The command version value is of unsigned integer type with 2 byte length, which mean calling -[NSNumber unsignedIntValue] to retrieve the value. The Least Significant Byte refer to minor version, and the Most Significant Byte refer to major version. i.e 0x0102 value represent a version of "1.2".
 *
 * You use this value to call methods selectively. Some methods is available to some versions.
 */
@property (nonatomic, readonly, nullable) NSNumber *cmdVersionNumber;

/**
 * Return the EQ related command version info of this device represented.
 *
 * @discussion The command version value is of 16-bit unsigned integer type, which mean calling -[NSNumber unsignedIntValue] to retrieve the value. The Least Significant Byte refer to minor version, and the Most Significant Byte refer to major version. i.e 0x0102 value represent a version of "1.2".
 *
 * You use this value to call methods selectively. Some methods is available to some versions.
 */
@property (nonatomic, readonly, nullable) NSNumber *EQVersionNumber;


/**
 * Get the Version of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update versionNumber and EQVersionNumber properties when succeed.
 */
- (void)getVersionInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint16_t cmdVer, uint16_t eqVer))handler;


/**
 * Get the Chip and Package information of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update chipID and packageID properties when succeed.
 */
- (void)getPackageInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproChip chip, NSUInteger packageID))handler;


/**
 * Return a boolean value that indicates if the capability information did determined.
 *
 * @return Return YES if the information did retrieved from peripheral. return NO if the message didn't receive or peripheral does not support report this information.
 */
@property (readonly) BOOL capabilitySettled;

/**
 * Return a boolean value that indicates whether the specified feature is available.
 *
 * @return Return YES if the specified feature is supported. return NO if the message didn't receive or peripheral does not support report this information.
 */
- (BOOL)isAvailableFor:(RTKBBproCapabilityType)capability;

/**
 * Get the feature realted information of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update capabilitySettled properties when succeed.
 */
- (void)getCapabilityWithCompletionHandler:(nullable RTKLECompletionBlock)handler;


/**
 * Check local saved state and send to peripheral if needed.
 *
 * @discussion For some state, the state in app should take priority if inconsistency exist between app and device, and app should send local state to device.
 */
- (void)syncLocalSavedStateToDevice;


#pragma mark - Product ID
/**
 * Get product information of the connected peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update companyID and modelID when succeed.
 */
- (void)getProductInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproEntityID modelId, RTKBBproEntityID companyId))handler;


#pragma mark - Device Name

/**
 * Get the LE name of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LEName property when succeed.
 */
- (void)getLENameWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSString *name))handler;

/**
 * Set a new LE name of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LEName property when succeed.
 */
- (void)setLEName:(NSString *)name withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the BREDR name of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update BREDRName property when succeed.
 */
- (void)getBREDRNameWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSString *name))handler;

/**
 * Set a new BREDR name of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update BREDRName property when succeed.
 */
- (void)setBREDRName:(NSString *)name withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;



#pragma mark - Language
/**
 * Get the current using language and supported languages of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update currentLanguage and supportedLanguages properties when succeed.
 */
- (void)getLanguageWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSSet <RTKBBproLanguageType>* supportedLangs, RTKBBproLanguageType currentLang))handler;

/**
 * Set the current language of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update currentLanguage property when succeed.
 */
- (void)setCurrentLanguage:(RTKBBproLanguageType)lang withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - Battery
/**
 * Get the current battery level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update primaryBatteryLevel and secondaryBatteryLevel and cradleBatteryLevel properties when succeed.
 */
- (void)getBatteryLevelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproBatteryLevel primary, RTKBBproBatteryLevel secondary, RTKBBproBatteryLevel cradle))handler;


#pragma mark - Legacy profile connection

- (void)connectProfile:(RTKBBproLegacyProfile)profile withCompletionHandler:(RTKLECompletionBlock)handler;

- (void)cancelProfileConnection:(RTKBBproLegacyProfile)profile withCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * Convert primary/secondary battery to left/right battery of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update leftBatteryLevel, rightBatteryLevel and singleBatteryLevel properties when succeed. (RWSState property should be ready before calling this method)
 */
- (void)convertBatteryToLeftRightFromPrimary:(RTKBBproBatteryLevel)primary andSecondary:(RTKBBproBatteryLevel)secondary WithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler;

#pragma mark - RWS

/**
 * Get the default role information of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update defaultBudRole property when succeed.
 */
- (void)getDefaultBudRoleWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproBudRole role))handler;


/**
 * Get the bud side information of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update budSide property when succeed.
 */
- (void)getBudSideWithCompletionHandler:(nullable void(^)(BOOL success, NSError *_Nullable err, RTKBBproBudSide side))handler;


/**
 * Get the default channel information of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update defaultChannelSide property when succeed.
 */
- (void)getRWSDefaultChannelSideWithCompletionHandler:(nullable void(^)(BOOL success, NSError *_Nullable error, RTKBBproChannelDirection direction))handler;


/**
 * Get the RWS on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSState property when succeed.
 */
- (void)getRWSStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL state))handler;

/**
 * Get the RWS channel flow state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSChannel property when succeed.
 */
- (void)getRWSChannelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproChannelDirection channel))handler;

/**
 * Switch the RWS channel state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSChannel property when succeed.
 */
- (void)switchRWSChannelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;



#pragma mark - APT
/**
 * Get the APT on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTState property when succeed.
 */
- (void)getAPTStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL state))handler;

/**
 * Switch the APT on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTState property when succeed.
 */
- (void)switchAPTStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;



#pragma mark - DSP
/**
 * Get DSP version information of BBpro Peripheral.
 * @param handler Called upon completion. If success, info is a dictionary containning DSP Info element (a example next), else a error is provided. info Dictionary example:  @{@"Scenario": @(0x00), @"SF": @(0x03), @"ROM": @(0x01010101), @"RAM": @(0x01010101), @"Patch": @(0x01010101), @"SDK": @(0x01010101)}
 *
 * @discussion Deprecated by more recent device.
 */
- (void)getDSPInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSDictionary <NSString*, NSNumber*>*_Nullable info))handler;


/**
 * Set DSP EQ effect parameter.
 *
 * @param paramterData EQ raw data to send to DSP. The data should be return by call -[RTKBBproEQSetting parameterDataOfSampleRate:] method.
 * @param handler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 *
 * @discussion This method set equalizer of 44.1kHz sample rate audio. Deprecated by more recent device.
 */
- (void)setDSPEQ:(NSData *)paramterData withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Set DSP EQ effect parameter.
 *
 * @param paramterData EQ raw data to send to DSP. The data should be return by call -[RTKBBproEQSetting parameterDataOfSampleRate:] method.
 * @param sampleRate The sample rate to which this EQ affect.
 * @param handler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 *
 * @discussion Deprecated by more recent device.
 */
- (void)setDSPEQData:(NSData *)paramterData ofSampleRate:(RTKBBproSampleRate)sampleRate withCompletionHandler:(nullable void(^)(BOOL, NSError*_Nullable))handler;

/**
 * Clear DSP equalizer effect.
 *
 * @param handler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 *
 * @discussion Deprecated by more recent device.
 */
- (void)clearDSPEQWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


#pragma mark - Listening mode

/**
 * Get the Listening mode cycle of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update listeningSwitchCycle property when succeed.
 */
- (void)getListeningModeSwitchCycleStateWithCompletionHandler:(nullable void (^)(BOOL success, NSError *_Nullable error, RTKBBproListeningModeSwitchCycle cycle))handler;

/**
 * Set the Listening mode cycle of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update listeningSwitchCycle property when succeed.
 */
- (void)setListeningSwitchCycle:(RTKBBproListeningModeSwitchCycle)cycle withCompletionHandler:(nullable RTKLECompletionBlock)handler;



#pragma mark - ANC

/**
 * Get the ANC related state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update ANCEnabled, currentANCModeIndex and ANCModes properties when succeed.
 */
- (void)getANCStateWithCompletionHandler:(nullable void (^)(BOOL success, NSError *_Nullable error, BOOL enabled, NSUInteger currentModeIndex, NSArray<NSNumber*> * modes))handler;

/**
 * Set the ANC on-off of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update ANCEnabled property when succeed.
 */
- (void)setANCEnable:(BOOL)enabled withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Set the ANC scenario of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update currentANCModeIndex property when succeed.
 */
- (void)setCurrentANCModeIndex:(NSUInteger)index withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Set the ANC on-off and scenario of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update ANCEnabled and currentANCModeIndex properties when succeed.
 */
- (void)setEnable:(BOOL)enabled ofANCModeIndex:(NSUInteger)index withCompletionHandler:(nullable RTKLECompletionBlock)handler;



#pragma mark - LLAPT

/**
 * Get the LLAPT related state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LLAPTEnabled, currentLLAPTScenarioIndex and LLAPTScenarios properties when succeed.
 */
- (void)getLLAPTStateWithCompletion:(nullable void (^)(BOOL success, NSError *_Nullable error, BOOL enabled, NSUInteger currentScenarioIndex, NSArray<NSNumber*> *scenarios))handler;

/**
 * Set the LLAPT on-off of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LLAPTEnabled property when succeed.
 */
- (void)setLLAPTEnable:(BOOL)enabled withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/**
 * Set the LLAPT scenario of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update currentLLAPTScenarioIndex property when succeed.
 */
- (void)setCurrentLLAPTScenarioIndex:(NSUInteger)index withCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * Set the LLAPT on-off and scenario of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LLAPTEnabled and currentLLAPTScenarioIndex properties when succeed.
 */
- (void)setEnable:(BOOL)enabled ofLLAPTScenarioIndex:(NSUInteger)index withCompletionHandler:(RTKLECompletionBlock)handler;

@end


#pragma mark - EQ

/**
 * EQ related operation.
 * Only available when version >= 1.0
 */
@interface RTKBBproPeripheral (EQ)

/**
 * Get EQ Setting index of peripheral
 *
 * @param handler This block is called upon completion. If the action take effect then success is YES and error is nil, and parameter currentIndex is the the EQ Setting Index current used in Peripheral, supportedIndexes is a bit field which indicate supported Indexes. Otherwise success is NO with an error.
 * @see RTKBBproEQIndex type
 */
- (void)getLegacyCurrentEQIndexStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproEQIndex currentIndex, RTKBBproEQIndex supportedIndexes))handler;

/**
 * Set current used EQ Setting of peripheral
 * @param index A bitmask whose EQ Setting to use bit set 1.
 * @see RTKBBproEQIndex type
 */
- (void)setLegacyCurrentEQIndexTo:(RTKBBproEQIndex)index withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;


/**
 * Get eq state information of the connected device.
 */
- (void)getEQStateInGamingMode:(BOOL)isGamingMode withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL isOn, NSUInteger EQEntryCount, NSUInteger currentEQIndex))handler;


@end



/**
 * Defines several comprehensive methods for get EQ related information and set eq setting parameter to remote device.
 *
 * @discussion All methods receive a mode agument which identify the voice control mode. When call methods other than -isEQEnabledOfMode:, the mode parameter should be exact the mode runing in the device, otherwise completionHandler is called with failure.
 */
@interface RTKBBproPeripheral (EQAccess)

/**
 * Get the count of EQ setting entrys of the specified mode.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)getEQEntryCountOfMode:(RTKBBproEQMode)mode
        withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger count))handler;

/**
 * Get the index of EQ setting entry current used of the specified mode.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)getCurrentEQIndexOfMode:(RTKBBproEQMode)mode
          withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger idx))handler;

/**
 * Set the index of EQ setting entry current used of the specified mode.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)setCurrentEQIndex:(NSUInteger)idx
                   ofMode:(RTKBBproEQMode)mode
    withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Get the parameter data of a EQ setting entry specified by index of the specified mode.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully. When succeed, a paraData is received along with sample rate.
 */
- (void)getEQParameterOfIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                   completionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproSampleRate sampleRate, NSData *_Nullable paraData))handler;

/**
 * Send the parameter data to a EQ setting entry specified by index of the specified mode.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)setEQParameterOfIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                     withData:(NSData *)data
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
            completionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Get the parameter data of a EQ setting entry specified by index of the specified mode and use the parameter data to make the RTKBBproEQSetting object full-fledged.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)getEQParameterAtIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
           toResolveEQSetting:(RTKBBproEQSetting *)setting
            completionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Set a EQ setting entry specified by index of the specified mode with a RTKBBproEQSetting object.
 *
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
- (void)setEQParameterAtIndex:(NSUInteger)idx
                       ofMode:(RTKBBproEQMode)mode
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                  withSetting:(RTKBBproEQSetting *)setting
            completionHandler:(nullable RTKLECompletionBlock)handler;

@end


#pragma mark - VP/Ringtone Volume
/**
 * The Volume category define several propropertied and methods for getting and setting volume related state.
 */
@interface RTKBBproPeripheral (Volume)

/**
 * Get the VP & Ringtone volume state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LVPRingtoneVolumeLevel, RVPRingtoneVolumeLevel, minLVPRingtoneVolumeLevel, maxLVPRingtoneVolumeLevel, minRVPRingtoneVolumeLevel, maxRVPRingtoneVolumeLevel and syncVPRingtoneVolume (if support) when succeed.
 */
- (void)getVPRingtoneVolumeStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproVolumeLevel minLevelL, RTKBBproVolumeLevel maxLevelL, RTKBBproVolumeLevel curLevelL, RTKBBproVolumeLevel minLevelR, RTKBBproVolumeLevel maxLevelR, RTKBBproVolumeLevel curLevelR))handler;

/**
 * Set the VP & Ringtone volume level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LVPRingtoneVolumeLevel, RVPRingtoneVolumeLevel properties when succeed.
 */
- (void)setVPRingtoneVolumeLevelOfL:(RTKBBproVolumeLevel)levelL R:(RTKBBproVolumeLevel)levelR withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Set VP & Ringtone volume level and sync info of this peripheral.
 * @param sync A boolean value indicates whether left volume and right volume are consistent.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LVPRingtoneVolumeLevel, RVPRingtoneVolumeLevel and syncVPRingtoneVolume properties when succeed.
 */
- (void)setVPRingtoneVolumeLevelOfL:(RTKBBproVolumeLevel)levelL R:(RTKBBproVolumeLevel)levelR Sync:(uint8_t)sync withCompletionHandler:(nullable RTKLECompletionBlock)handler;

@end


#pragma mark - MMI
/**
 * The MMI category defines several methods for getting MMI related information or send MMI action.
 */
@interface RTKBBproPeripheral (MMI)

/**
 * Send a MMI action to the device.
 */
- (void)sendMMIAction:(RTKBBproPeripheralMMI)action withCompletionHandler:(nullable RTKLECompletionBlock)handler;


/**
 * Get the supported MMI actions of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update supportedMMIs property when succeed.
 */
- (void)getSupportedMMIsWithCompletionHandler:(nullable RTKLECompletionBlock)handler;


/**
 * Get the supported MMI click motions of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update supportedClicks property when succeed.
 */
- (void)getSupportedClicksWithCompletionHandler:(nullable RTKLECompletionBlock)handler;


/**
 * Get the supported call status for MMI of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update supportedPhoneStatus property when succeed.
 */
- (void)getSupportedPhoneStatusWithCompletionHandler:(nullable RTKLECompletionBlock)handler;


/**
 * Get the MMI mapping configured of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update MMIKeyMappings property when succeed.
 */
- (void)getMMIKeyMappingWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Set a MMI mapping configuration to this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update MMIKeyMappings property when succeed.
 */
- (void)setMMIKeyMapping:(RTKBBproMMIMapping)mapping withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Reset the MMI mapping to the default configuration.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update MMIKeyMappings property when succeed.
 */
- (void)resetMMIKeyMappingWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Reset the MMI mapping to the default configuration.(Distinguish left and right)
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 * @param budSide 0x00->stereo 0x01->left 0x02->right 0x03->left and right
 * @discussion Will update MMIKeyMappings property when succeed.
 */
- (void)resetMMIKeyMappingOfBudSide:(uint8_t)budSide withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Get the Button Locked on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update buttonLocked property when succeed.
 */
- (void)getMMIButtonLockStateWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Switch the Button Locked on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update buttonLocked property when succeed.
 */
- (void)switchMMIButtonLockStateWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Request the device to enter to Pairing mode.
 */
- (void)requestEnterToPairingModeWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Request the device to exit from Pairing mode.
 */
- (void)requestExitToPairingModeWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Send a power off MMI action to this peripheral.
 */
- (void)powerOffWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Get the RWS MMI mapping configuration of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSMMIKeyMappings property when succeed.
 */
- (void)getRWSKeyMMIMapWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Configure a RWS MMI mapping of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSMMIKeyMappings property when succeed.
 */
- (void)setRWSKeyMMIMap:(RTKBBproMMIMapping)mapping withCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Reset the RWS MMI mapping to the default configuration.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update RWSMMIKeyMappings property when succeed.
 */
- (void)resetRWSKeyMMIMapWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Reset the RWS MMI mapping to the default configuration.(Distinguish left and right)
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 * @param budSide 0x00->stereo 0x01->left 0x02->right 0x03->left and right
 * @discussion Will update RWSMMIKeyMappings property when succeed.
 */
- (void)resetRWSKeyMMIMapOfBudSide:(uint8_t)budSide WithCompletionHandler:(nullable RTKLECompletionBlock)handler;
@end


#pragma mark - APTGain
/**
 * The APTGain category defines several properties and methods for access APT Noice Reduction related state.
 */
@interface RTKBBproPeripheral (APTGain)

/**
 * Return whether the APT Noice Reduction is on of this peripheral last cached.
 *
 * @discussion Scalar value is of BOOL type.
 */
@property (nonatomic, readonly, nullable) NSNumber *APTNRStatus;

/**
 * Get the APT NR on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTNRStatus property when succeed.
 */
- (void)getAPTNRStatusWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Switch the APT NR on-off state of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTNRStatus property when succeed.
 */
- (void)switchAPTNROnOffWithCompletion:(RTKLECompletionBlock)completionHandler;


/**
 * Return the APT Noice Reduction gain level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type.
 */
@property (nonatomic, readonly, nullable) NSNumber *APTGainLevel;

/**
 * Return the maximum APT Noice Reduction gain level of this peripheral last cached.
 *
 * @discussion Scalar value is of unsigned integer type. This property is not KVO applicable.
 */
@property (nonatomic, readonly, nullable) NSNumber *maximumAPTGainLevel;

/**
 * Get the APT Gain level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTGainLevel, maximumAPTGainLevel property when succeed.
 */
- (void)getAPTGainLevelWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, NSUInteger gain, NSUInteger maxGain))handler;


/**
 * Set the APT Gain level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTGainLevel property when succeed.
 */
- (void)setAPTGainLevel:(NSUInteger)level withCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * Set the APT Gain level by increase of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTGainLevel property when succeed.
 */
- (void)increaseAPTGainLevelWithCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * Set the APT Gain level by decrease of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTGainLevel property when succeed.
 */
- (void)decreaseAPTGainLevelWithCompletionHandler:(RTKLECompletionBlock)handler;

@end


#pragma mark - LowLatency
/**
 * The LowLatency category defines methods for access low latency related state of this peripheral.
 */
@interface RTKBBproPeripheral (LowLatency)

/**
 * Switch the gaming mode on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update gamingModeEnabled property when succeed.
 */
- (void)switchGamingModeEnableWithCompletionHandler:(nullable RTKLECompletionBlock)handler;

/**
 * Get the Low latency state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update lowLatencyLevel, maxLowLatencyLevel and lowLatencyValue properties when succeed.
 */
- (void)getLowLatencyStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError *err, BOOL enabled, NSUInteger value, NSUInteger level, NSUInteger maxLevel))handler;

/**
 * Get the Low latency level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update lowLatencyLevel and lowLatencyValue properties when succeed.
 */
- (void)setLowLatencyLevel:(NSUInteger)level withCompletionHandler:(nullable RTKLECompletionBlock)handler;

@end


#pragma mark - APT EQ

@interface RTKBBproPeripheral(APTEQ)
/**
 * Get APT EQ settings of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTEQCount property and initialize APTEQSettingsOfLeft,  APTEQSettingsOfRight, APTEQSettings when succeed.
 */
- (void)getAPTEQSettingsWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger APTEQCount))handler;

/**
 * Get APT EQ count of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTEQCount property when succeed.
 */

- (void)getAPTModeEQEntryCountWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger APTEQCount))handler;

/**
 * Get current APT EQ index of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTEQIndex when succeed.
 */
- (void)getCurrentAPTEQIndexWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger APTEQIndex))handler;

/**
 * Set current APT EQ index of this peripheral.
 * @param completionHandler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTEQIndex when succeed.
 */
- (void)setCurrentAPTEQIndex:(NSUInteger)index completion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler;

/**
 * Get the specified APT EQ parameters of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the specified APTEQSettings when succeed.
 */
- (void)getEQParameterOfIndex:(NSUInteger)index andType:(RTKBBproSWEQType)type inMode:(RTKBBproEQMode)mode completionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproSampleRate sampleRate, NSData *paraData))handler;

/**
 * Set the specified APT EQ parameters of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the specified APTEQSettings(left/right) when succeed.
 */
- (void)setEQParameterOfIndex:(NSUInteger)index
                      andType:(RTKBBproSWEQType)type
                       inMode:(RTKBBproEQMode)mode
                     withData:(NSData *)data
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                       forBud:(RTKEQBudSide)bud
            completionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the specified APT EQ parameters of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the specified APTEQSettings when succeed.
 */
- (void)getEQParameterAtIndex:(NSUInteger)idx ofType:(RTKBBproSWEQType)type inMode:(RTKBBproEQMode)mode toResolveEQSettings:(NSArray <RTKBBproEQSettingPlaceholder*> *)settings completionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the specified APT EQ parameters of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the specified APTEQSettings when succeed.
 */
- (void)getEQParameterAtIndex:(NSUInteger)idx ofType:(RTKBBproSWEQType)type inMode:(RTKBBproEQMode)mode toResolveEQSetting:(RTKBBproEQSetting *)setting completionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Set the specified APT EQ parameters of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the specified APTEQSettings(left/right) when succeed.
 */
- (void)setEQParameterAtIndex:(NSUInteger)idx
                       ofType:(RTKBBproSWEQType)type
                       inMode:(RTKBBproEQMode)mode
                 ofSampleRate:(RTKBBproSampleRate)sampleRate
                  withSetting:(RTKBBproEQSetting *)setting
                       forBud:(RTKEQBudSide)bud
            completionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end


#pragma mark - EQ (SPEC 2.0)
@interface  RTKBBproPeripheral(EQ0200)
/**
 * Get EQ info(count, index mapping, sampleRate) of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the numberOfSavedSPKEQ, numberOfSavedMICEQ, normalEQIndexArray, gamingEQIndexArray, ancEQIndexArray, SPKEQSettings, currentNormalEQIndex, currentGamingEQIndex, currentANCEQIndex, currentSampleRate, supportedSampleRate, currentSPKEQMode, currentMICEQMode, APTEQSettings, APTEQSettingsOfLeft or APTEQSettingsOfRight when succeed.
 */
- (void)getEQInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Reset the specified EQ to the factory setting.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 * @param index The current EQ mode index.
 *
 * @discussion Will update the SPKEQSettings, APTEQSettings, APTEQSettingsOfLeft or APTEQSettingsOfRight when succeed.
 */
- (void)resetEQOfType:(RTKBBproSWEQType)type
                 mode:(RTKBBproEQMode)mode
                index:(uint8_t)index
              andSide:(RTKEQBudSide)side
   toResolveEQSetting:(RTKBBproEQSetting *)setting
withCompletionHandler:(nullable void(^)(BOOL success, NSError * _Nullable error, RTKBBproSampleRate sampleRate, NSData * _Nullable paraData))handler;

/**
 * Set the specified EQ parameters.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 * @param applyOrSave apply:0x00 save:0x01
 *
 * @discussion Will update the SPKEQSettings, APTEQSettings, APTEQSettingsOfLeft or APTEQSettingsOfRight when succeed.
 */
- (void)setEQParameterAtIndex:(NSUInteger)idx
                       ofType:(RTKBBproSWEQType)type
                       mode:(RTKBBproEQMode)mode
                 sampleRate:(RTKBBproSampleRate)sampleRate
                  withSetting:(RTKBBproEQSetting *)setting
                       forBud:(RTKEQBudSide)bud
                  applyOrSave:(uint8_t)applyOrSave
        withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the specified EQ parameters.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update the SPKEQSettings, APTEQSettings, APTEQSettingsOfLeft or APTEQSettingsOfRight when succeed.
 */
- (void)getEQParameterAtIndex:(NSUInteger)idx
                       ofType:(RTKBBproSWEQType)type
                         mode:(RTKBBproEQMode)mode
                          bud:(RTKEQBudSide)bud
           toResolveEQSetting:(RTKBBproEQSetting *)setting
        withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end

#pragma mark - APT Volume and Brightness

@interface  RTKBBproPeripheral(APTVolumeAndBrightness)

/**
 * Get the APT Volume state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update maxMainAPTVolumeLevel, maxSubAPTVolumeLevel, leftMainAPTVolumeLevel, leftSubAPTVolumeLevel, rightMainAPTVolumeLevel, rightSubAPTVolumeLevel and syncAPTVolume (if support) properties when succeed.
 */
- (void)getAPTVolumeInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t maxMainLevel, uint16_t maxSubLevel, uint8_t leftMainLevel, uint16_t leftSubLevel, uint8_t rightMainLevel, uint8_t rightSubLevel))handler;

/**
 * Set current APT Volume level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update leftMainAPTVolumeLevel, leftSubAPTVolumeLevel, rightMainAPTVolumeLevel and rightSubAPTVolumeLevel properties when succeed.
 */
- (void)setAPTVolumeLevelofType:(uint8_t)volumeType withLeftVolume:(uint16_t)leftVolume andRightVolume:(uint16_t)rightVolume withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the APT Volume state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update leftMainAPTVolumeLevel, leftSubAPTVolumeLevel, rightMainAPTVolumeLevel and rightSubAPTVolumeLevel properties when succeed.
 */
- (void)getAPTVolumeStatusWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t leftMainLevel, uint16_t leftSubLevel, uint8_t rightMainLevel, uint8_t rightSubLevel))handler;

/**
 * Get the APT Volume Sync state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update syncAPTVolume when succeed.
 */
- (void)getAPTVolumeSyncStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Switch the APT Volume Sync state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update syncAPTVolume when succeed.
 */
- (void)switchAPTVolumeSyncStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the APT brightness info of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update maxMainBrightnessLevel, maxSubBrightnessLevel, leftMainBrightnessLevel, leftSubBrightnessLevel, rightMainBrightnessLevel,  rightSubBrightnessLevel  and syncAPTBrightness (if support) properties when succeed.
 */
- (void)getAPTBrightnessInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t maxMainLevel, uint16_t maxSubLevel, uint8_t leftMainLevel, uint16_t leftSubLevel, uint8_t rightMainLevel, uint8_t rightSubLevel))handler;

/**
 * Set current APT brightness level of this peripheral.
 * @param type RTKBBproAPTVolumeType
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update leftMainAPTVolumeLevel, leftSubAPTVolumeLevel, rightMainAPTVolumeLevel and rightSubAPTVolumeLevel properties when succeed.
 */
- (void)setAPTBrightnessLevelofType:(uint8_t)type withLeftBrightness:(uint16_t)leftBrightness andRightBrightness:(uint16_t)rightBrightness withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Set current APT brightness level of this peripheral.
 *@param sync A boolean value indicates whether left and right brightness level are consistent.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update leftMainAPTVolumeLevel, leftSubAPTVolumeLevel, rightMainAPTVolumeLevel and rightSubAPTVolumeLevel and syncAPTBrightness properties when succeed.
 */
- (void)setAPTBrightnessLevelofType:(uint8_t)type withLeftBrightness:(uint16_t)leftBrightness andRightBrightness:(uint16_t)rightBrightness Sync:(uint8_t)sync withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get current APT brightness status of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update leftMainBrightnessLevel, leftSubBrightnessLevel, rightMainBrightnessLevel,  rightSubBrightnessLevel  and syncAPTBrightness (if support) properties when succeed.
 */
- (void)getAPTBrightnessStatusWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t leftMainLevel, uint16_t leftSubLevel, uint8_t rightMainLevel, uint8_t rightSubLevel))handler;

//versionNumber = 0x0104, 0x0105
/**
 * Set current APT Volume level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTVolumeOutLevel when succeed.
 */
-(void)setAPTVolumeOutLevel:(uint8_t)APTVolumeOutLevel withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get current APT Volume level of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTVolumeOutLevel when succeed.
 */
-(void)getAPTVolumeOutLevelWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t volumeLevel))handler;

@end


@interface  RTKBBproPeripheral(Multilink)

/**
 * Get the connection count of this peripheral which support multilink.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion A Multi-link supported device may estabilish connection with multiple devices.
 */
- (void)getAppConnectionCountWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, NSUInteger count))handler;

@end

#pragma mark - APT NR

@interface RTKBBproPeripheral(APTNR)

/**
 * Set the APT NR on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTNRState property when succeed.
 */
-(void)setAPTNRState:(uint8_t)onOrOff withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get the APT NR on-off state of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTNRState property when succeed.
 */
-(void)getAPTNRStateWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t state))handler;

@end

#pragma mark - APT Power On DelayTime

@interface RTKBBproPeripheral(APTPowerOnDelayTime)

/**
 * Get the delay time of APT is on after device power on of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTDelayTime property when succeed.
 */
-(void)getAPTPowerOnDelayTimeWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, uint8_t delayTime))handler;

/**
 * Set the delay time of APT is on after device power on of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update APTDelayTime property when succeed.
 */
-(void)setAPTPowerOnDelayTime:(uint8_t)delayTime withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end

#pragma mark - LLAPT Scenario

@interface RTKBBproPeripheral(LLAPTScenario)
/**
 * Get LLAPT Scenario info of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update LLAPTGroupsNumber and scenarios when succeed.
 */
-(void)getLLAPTScenarioChooseInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error,NSInteger groupsNumber, NSArray<NSNumber *> *scenarios))handler;

/**
 * Listen to the effect of the selected scenario.
 * @param LEnable 0x00 LLAPT disable 0x01 LLAPT enable
 * @param REnable 0x00 LLAPT disable 0x01 LLAPT enable
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
-(void)LLAPTScenarioChooseTryofLChannelType:(uint8_t)LEnable andLChannelIndex:(uint8_t)LIndex andRChannelType:(uint8_t)REnable andRChannelIndex:(uint8_t)RIndex withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get LLAPT scenario result after modifing the group settings of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
-(void)getLLAPTScenarioChooseResultofLScenarioList:(uint32_t)LScenarioList andRScenarioList:(uint32_t)RScenarioList withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get LLAPT scenario result after tring the effect but not saving the modified settings.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
-(void)getLLAPTScenarioChooseResultWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end


#pragma mark - ANC Scenario

@interface RTKBBproPeripheral(ANCScenario)
/**
 * Get ANC Scenario info of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update ANCGroupsNumber and ANCScenarios when succeed.
 */
-(void)getANCScenarioChooseInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error,NSInteger groupsNumber, NSArray<NSNumber *> *scenarios))handler;

/**
 * Listen to the effect of the selected scenario.
 * @param LEnable 0x00 ANC disable 0x01 ANC enable
 * @param REnable 0x00 ANC disable 0x01 ANC enable
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
-(void)tryANCScenarioChooseofLChannelType:(uint8_t)LEnable andLChannelIndex:(uint8_t)LIndex andRChannelType:(uint8_t)REnable andRChannelIndex:(uint8_t)RIndex withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get ANC scenario result after modifing the group settings of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
-(void)getANCScenarioChooseResultofLScenarioList:(uint32_t)LScenarioList andRScenarioList:(uint32_t)RScenarioList withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get ANC scenario result after tring the effect but not saving the modified settings.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 */
-(void)getANCScenarioChooseResultWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end




@interface RTKBBproPeripheral (Cache)

@property (nonatomic, readonly) RTKBBproPeripheralStateCache *cache;

@end


@interface RTKBBproPeripheral(BudInfo)
/**
 * Get bud info of this peripheral.
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update budType,  primaryBudSide, LRChannel, singleOrLeftBattery, rightBattery, RWSState and caseBattery (if support) when succeed.
 */
-(void)getBudInfoWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end


@interface RTKBBproPeripheral(ListeningModeReport)
/**
 * Set listening mode of this peripheral.
 * @param type 0x00 all off, 0x01 normal APT, 0x02 ANC, 0x03 LLAPT
 * @param index if type = 0x00 or 0x01, index = 0; if type = 0x02, index = ANCModeIndex; if type = 0x03, index = LLAPTScenarioIndex
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update ANCEnabled, APTState, LLAPTEnabled, currentANCModeIndex and currentLLAPTScenarioIndex when succeed.
 */
-(void)setListeningStateofType:(uint8_t)type andIndex:(uint8_t)index withCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

/**
 * Get listening mode of this peripheral. 
 * @param handler A block to called when this task complete successfullly or unsuccessfully.
 *
 * @discussion Will update ANCEnabled, APTState, LLAPTEnabled, currentANCModeIndex and currentLLAPTScenarioIndex when succeed.
 */
-(void)getListeningStateStatusWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error))handler;

@end
NS_ASSUME_NONNULL_END
