//
//  RTKBBproPeripheral+CustomeReadWrite.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2019/4/12.
//  Copyright © 2019 Realtek. All rights reserved.
//

#import <RTKAudioConnectSDK/RTKBBproPeripheral.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * This category provides property(properties) and method(s) for sending customized message bytes and receiving customized message bytes to or from a remote device.
 *
 * @discussion As suggest, don't use APIs defined here to send messages or receiving messages already known by SDK. SDK will process income messages it  recognize.
 */
@interface RTKBBproPeripheral (CustomReadWrite) <RTKPackageCommunicationClient>

/**
 * Return a communication object for used to send and receive customized message.
 */
@property (readonly) RTKRequestCommunication *communication;

@end

NS_ASSUME_NONNULL_END
