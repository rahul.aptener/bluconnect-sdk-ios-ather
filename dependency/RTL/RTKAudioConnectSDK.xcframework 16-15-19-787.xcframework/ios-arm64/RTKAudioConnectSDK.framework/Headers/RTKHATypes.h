//
//  RTKHATypes.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2023/2/24.
//

#ifndef RTKHATypes_h
#define RTKHATypes_h

/// A relative value which represents a volume level.
///
/// Having range of 0..100.
typedef uint8_t HAVolumeLevel;

/// A relative value represents a EQ gain level.
///
/// Having a range of 0...100.
typedef uint8_t HAGainLevel;

///
typedef uint8_t HABalanceType;

/// Type used to represent a gain, specified in DB.
///
/// Havign a range of -20...30.
typedef double HAVolumeGain;

/// Type used to represent a gain, specified in DB.
///
/// Havign a range of -20...60.
typedef double HAEQGain;

/// Represent an audio frequency in Hz.
///
/// Havign a range of 0...12000.
typedef double HAFrequency;

typedef double RTKACHADecibel;

typedef enum {
    RTKACHAEar_left,
    RTKACHAEar_right,
} RTKACHAEar;

@interface RTKACHAPitchThreshold : NSObject
@property (readonly) RTKACHAEar ear;
@property (readonly) HAFrequency freq;
@property (readonly) RTKACHADecibel db;

- (instancetype)initWithEar:(RTKACHAEar)ear frequency:(HAFrequency)freq threshold:(RTKACHADecibel)db;
@end

/// Contants that represent Hearing Aid Noice Reduction mode.
typedef NS_ENUM(uint8_t, RTKHANRMode) {
    RTKHANR_lowLatency      =   0x00,   ///< Low latency
    RTKHANR_neural          =   0x02,   ///< Neural
    RTKHANR_highLatency     =   0x03,   ///< High Latency
};

typedef struct {
    int8_t volMaxGain;
    int8_t volMinGain;
    uint8_t balanceGain;
    uint8_t eqChCount;
    uint16_t eqChFreqs[32];
    int8_t eqChMaxGains[32];
    int8_t eqChMinGains[32];
} RTKHAGainConfiguration;

/// Constants that indicates current volume mute state.
typedef NS_ENUM(uint8_t, RTKHAAPTVolumeMuteState) {
    RTKHAAPTVolumeMuteState_unmute      =   0x00,
    RTKHAAPTVolumeMuteState_mute,
    RTKHAAPTVolumeMuteState_invalid,
};

typedef NS_ENUM(uint8_t, RTKHAApplyBud) {
    RTKHAApplyBud_left      =   0x00,
    RTKHAApplyBud_right,
    RTKHAApplyBud_both,
};

typedef struct {
    uint setCount;
    uint attackTime;
    uint releaseTime;
    struct {
        int threshold;
        float slope;
    } sets[2];
} RTKHADRCState;

#endif /* RTKHATypes_h */
