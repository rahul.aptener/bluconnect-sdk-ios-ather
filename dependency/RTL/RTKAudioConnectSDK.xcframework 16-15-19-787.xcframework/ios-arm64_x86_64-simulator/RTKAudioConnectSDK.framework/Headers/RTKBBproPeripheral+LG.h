//
//  RTKBBproPeripheral+LG.h
//  RTKAudioConnectSDK
//
//  Created by jerome_gu on 2021/9/18.
//

#import <RTKAudioConnectSDK/RTKBBproPeripheral.h>

NS_ASSUME_NONNULL_BEGIN


/**
 * This category defines APIs for communication with LG dedicated SOC implementation.
 */
@interface RTKBBproPeripheral (LG)

@property (nonatomic, nullable) NSNumber *vibrationEnable;


/**
 *  The last received ANCS event from peripheral.
 * @discussion The lastANCSEvent and lastANCSAttr are the last ANCS  received from peripheral. This is a readonly property. If you want to be notified when receive new ANCS event, use KVO approach.
 */
@property (nonatomic, readonly, nullable) RTKANCSNotificationEvent *lastANCSEvent;

/**
 * The last received ANCS event from peripheral.
 *
 * @discussion The lastANCSEvent and lastANCSAttr are the last ANCS  received from peripheral. This is a readonly property. If you want to be notified when receive new ANCS event, use KVO approach.
*/
@property (nonatomic, readonly, nullable) RTKANCSNotificationAttribute *lastANCSAttr;


// The whisper mode.
typedef enum {
    RTKBBproWhisperMode_unknown = -1,
    RTKBBproWhisperMode_off,
    RTKBBproWhisperMode_onRight,
    RTKBBproWhisperMode_onLeft,
} RTKBBproWhisperMode;

/**
 * The whisper mode current known.
 * This may not reflect to the actual state on a remote peripheral. If the actual state did not retrieved before, the property return a meaningless null value (RTKBBproWhisperMode_unknown), and auto start a underlying message exchange to get the actual state.
 */
@property (nonatomic, readonly) NSNumber *whisperMode;

/**
 * Get peripheral whisper mode asynchronously.
 * If this method succeed, the RTKBBproPeripheral.whisperMode will get update, and related KVO notification is send.
 */
- (void)getWhisperModeStateWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, RTKBBproWhisperMode mode))handler;

/**
 * Set peripheral whisper mode asynchronously.
 * If this method succeed, the RTKBBproPeripheral.whisperMode will get update, and related KVO notification is send.
 */
- (void)setWhisperMode:(RTKBBproWhisperMode)mode withCompletionHandler:(RTKLECompletionBlock)handler;



typedef enum {
    RTKBBproAPTMode_unknown = -1,
    RTKBBproAPTMode_normal,
    RTKBBproAPTMode_talkThrough,
} RTKBBproAPTMode;

/**
 * The APT mode current known.
 * This may not reflect to the actual state on a remote peripheral. If the actual state did not retrieved before, the property return a meaningless null value (RTKBBproAPTMode_unknown), and auto start a underlying message exchange to get the actual state.
 */
@property (nonatomic, readonly) NSNumber *APTMode;

/**
 * Get peripheral whisper mode asynchronously.
 * If this method succeed, the RTKBBproPeripheral.APTMode will get update, and related KVO notification is send.
 */
- (void)getAPTModeWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, RTKBBproAPTMode mode))handler;

/**
 * Set peripheral APT mode asynchronously.
 * If this method succeed, the RTKBBproPeripheral.APTMode will get update, and related KVO notification is send.
 */
- (void)setAPTMode:(RTKBBproAPTMode)mode withCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * The Auto Play state current known.
 * This may not reflect to the actual state on a remote peripheral. If the actual state did not retrieved before, the property return a meaningless null value (NO), and auto start a underlying message exchange to get the actual state.
 */
@property (nonatomic, readonly) NSNumber *autoPlayEnabled;

/**
 * Get peripheral auto play state asynchronously.
 * If this method succeed, the RTKBBproPeripheral.autoPlayEnabled will get update, and related KVO notification is send.
 */
- (void)getAutoPlayStateWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, BOOL enabled))handler;

/**
 * Set peripheral auto play state asynchronously.
 * If this method succeed, the RTKBBproPeripheral.autoPlayEnabled will get update, and related KVO notification is send.
 */
- (void)setAutoPlayEnable:(BOOL)enabled withCompletionHandler:(RTKLECompletionBlock)handler;


typedef struct {
    struct {
        uint8_t major;
        uint8_t minor;
        uint8_t patch;
        uint16_t build;
    } app;
    struct {
        uint8_t major;
        uint8_t minor;
        uint16_t build;
    } patch;
    struct {
        uint8_t major;
        uint8_t minor;
        uint8_t patch;
        uint8_t revision;
        uint16_t build;
        uint8_t blank[10];
    } MCUConfigTool;
    struct {
        uint8_t major;
        uint8_t minor;
        uint8_t patch;
        uint8_t revision;
        uint16_t build;
    } DSPConfigTool;
    uint32_t dspApp;
    uint32_t dspSys;
} FWBinVersions;

/**
 * Get FW version info asynchronously.
 */
- (void)getFWVersionWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, FWBinVersions versions))handler;



#pragma mark -  EQ Index

@property (nonatomic, nullable) NSNumber *currentEQIndex DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");
@property (nonatomic, readonly, nullable) NSNumber *supportedEQIndexes DEPRECATED_MSG_ATTRIBUTE("Use cache property instead.");

/**
 * Get EQ Setting index of peripheral
 *
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil, and parameter currentIndex is the the EQ Setting Index current used in Peripheral, supportedIndexes is a bit field which indicate supported Indexes. Otherwise success is NO with an error.
 * @see RTKBBproEQIndex type
 */
- (void)getEQIndexStateWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, RTKBBproEQIndex currentIndex, RTKBBproEQIndex supportedIndexes))completionHandler;

/**
 * Set current used EQ Setting of peripheral
 * @param index A bitmask whose EQ Setting to use bit set 1.
 * @see RTKBBproEQIndex type
 */
- (void)setEQIndex:(RTKBBproEQIndex)index withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler;


#pragma mark - Vibration
/**
 * Get Peripheral Vibration Status
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil, and state indicate whether vibration is on. Otherwise success is NO with an error.
 */
- (void)getVibrationStatusWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL state))completionHandler;

/**
 *
 */
- (void)getVibrationModeWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error, uint16_t onTime, uint16_t offTime, uint8_t count))handler;

/**
 * Enable or disable Vibration
 * @param enabled Control whether peripheral vibration function enable.
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 */
- (void)setVibrationEnable:(BOOL)enabled withCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler;

/**
 * Toggle vibration function
 *
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 */
- (void)toggleVibrationWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler;

/**
 * Stop in progress vibration
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 * @discussion Only function when a happening vibration
 */
- (void)stopVibrationWithCompletion:(nullable void(^)(BOOL success, NSError*_Nullable error))completionHandler;

/**
 * Set vibration behavior pattern
 * @param on On period time (10ms/uinit)
 * @param off Off period time (10ms/uinit)
 * @param count Vibrate repeat count
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 */
- (void)setVibrationModeWithOnPeriod:(NSUInteger)on offPeriod:(NSUInteger)off repeat:(NSUInteger)count completion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler;


#pragma mark - ANCS

/**
 * Make SOC to use ANCS
 * @param completionHandler This block is called upon completion. If the action take effect then success is YES and error is nil. Otherwise success is NO with an error.
 * @discussion Only function when a happening vibration
 */
- (void)registerANCSWithCompletion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler;


#pragma mark - Find me
/**
 * The find me status of peripheral. This may trigger send a get requrest to peripheral.
 * This property is KVO-capable.
 */
@property (nonatomic, nullable, readonly) NSNumber *leftBudFindmeEnabled;
@property (nonatomic, nullable, readonly) NSNumber *rightBudFindmeEnabled;

/**
 * Start get the find me status of remote peripheral.
 */
- (void)getFindmeStatusWithCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * Set find me status.
 */
- (void)setFindmeState:(BOOL)isOn ofRightBud:(BOOL)isRightBud withCompletionHandler:(RTKLECompletionBlock)handler;


#pragma mark - Customized feature
- (void)getSerialNumberDataWithCompletionHandler:(void(^)(BOOL success, NSError*_Nullable error, NSData *readedData))handler;


/**
 * Indicate whether peripehral is in voice notifying status.
 *
 * @discussion Add a KVO observer if you want to be notified when value change.
 */
@property (nonatomic, readonly) NSNumber *isVoiceNotifying;

/**
 * Notify peripheral that voice notificaiton will begin.
 */
- (void)indicateVoiceNotificationStartWithCompletionHandler:(RTKLECompletionBlock)handler;

/**
 * Notify peripheral that voice notificaiton will end.
*/
- (void)indicateVoiceNotificationTerminationWithCompletionHandler:(RTKLECompletionBlock)handler;


#pragma mark - Meridian sound effect
@property (nonatomic, readonly) NSNumber *meridianEffectMode;

- (void)getMeridianSoundEffectWithCompletionHandler:(RTKLECompletionBlock)handler;

- (void)setMeridianSoundEffectModeTo:(RTKBBproMeridianSoundEffect)mode withCompletion:(void(^)(BOOL success, NSError*_Nullable error))completionHandler;



// Boolean type. YES - on, NO - off.
@property (nonatomic, readonly) NSNumber *inEarDetectionStatus;

- (void)getInEarDetectionStatusWithCompletionHandler:(nullable void(^)(BOOL success, NSError*_Nullable error, BOOL isOn))handler;

- (void)switchInEarDetectionStatusWithCompletionHandler:(RTKLECompletionBlock)handler;

@end

NS_ASSUME_NONNULL_END
