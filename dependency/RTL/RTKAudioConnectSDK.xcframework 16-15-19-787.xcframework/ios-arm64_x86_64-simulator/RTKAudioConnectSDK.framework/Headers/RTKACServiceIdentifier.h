//
//  RTKACServiceIdentifier.h
//  RTKLEFoundation
//
//  Created by jerome_gu on 2019/4/10.
//  Copyright © 2019 Realtek. All rights reserved.
//

#ifndef RTKACServiceIdentifier_h
#define RTKACServiceIdentifier_h

#define SERVICE_ID_BBPRO    @"000002FD-3C17-D293-8E48-14FE2E4DA212"
#define CHAR_ID_BBPRO_TX    @"FD03"
#define CHAR_ID_BBPRO_RX    @"FD04"

#define ADV_SERVICE_ID_PRIMARY    @"010002FD-3C17-D293-8E48-14FE2E4DA212"
#define ADV_SERVICE_ID_SECONDARY    @"020002FD-3C17-D293-8E48-14FE2E4DA212"

#endif /* RTKACServiceIdentifier_h */
